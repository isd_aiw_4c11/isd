-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2015 at 09:43 AM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fit_isd_2014`
--
CREATE DATABASE IF NOT EXISTS `fit_isd_2014` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `fit_isd_2014`;

-- --------------------------------------------------------

--
-- Table structure for table `absence_request`
--

CREATE TABLE `absence_request` (
  `abr_id` int(11) NOT NULL AUTO_INCREMENT,
  `abr_staff_id` int(11) NOT NULL,
  `abr_date` date NOT NULL,
  `abr_reason` text COLLATE utf8_unicode_ci NOT NULL,
  `abr_is_approved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`abr_id`,`abr_staff_id`),
  KEY `fk_absence_request_staff1_idx` (`abr_staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `absence_request`
--

INSERT INTO `absence_request` (`abr_id`, `abr_staff_id`, `abr_date`, `abr_reason`, `abr_is_approved`) VALUES
(1, 3, '2015-01-02', 'Xin phép cho em được nghỉ ngày mai. Gia đình có việc bận.', 1),
(3, 4, '2015-01-12', 'Xin phép nghỉ 3 ngày từ 18-21/12/2015.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `advance_request`
--

CREATE TABLE `advance_request` (
  `adr_id` int(11) NOT NULL AUTO_INCREMENT,
  `adr_staff_id` int(11) NOT NULL,
  `adr_date` date NOT NULL,
  `adr_amount` float NOT NULL,
  `adr_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adr_id`),
  KEY `fk_advance_request_staff1_idx` (`adr_staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `advance_request`
--

INSERT INTO `advance_request` (`adr_id`, `adr_staff_id`, `adr_date`, `adr_amount`, `adr_status`) VALUES
(1, 1, '2014-12-28', 500000, 2),
(2, 2, '2014-12-28', 1000000, 1),
(5, 3, '2014-12-28', 150000, 2),
(6, 3, '2014-12-28', 150000, 1),
(7, 1, '2014-01-05', 120000, 2),
(8, 1, '2015-01-05', 120000, 2),
(9, 5, '2015-01-01', 500000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `att_id` int(11) NOT NULL AUTO_INCREMENT,
  `att_staff_id` int(11) NOT NULL,
  `att_date` date NOT NULL,
  `att_point` float NOT NULL DEFAULT '0',
  `att_extra_point` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`att_id`),
  KEY `att_staff_id` (`att_staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=451 ;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`att_id`, `att_staff_id`, `att_date`, `att_point`, `att_extra_point`) VALUES
(1, 1, '2014-12-01', 8, 2),
(2, 1, '2014-12-02', 8, 0),
(3, 1, '2014-12-03', 8, 0),
(4, 1, '2014-12-04', 8, 0),
(5, 1, '2014-12-05', 8, 0),
(6, 1, '2014-12-06', 8, 0),
(7, 1, '2014-12-07', 8, 0),
(8, 1, '2014-12-08', 8, 0),
(9, 1, '2014-12-09', 8, 0),
(10, 1, '2014-12-10', 8, 0),
(11, 1, '2014-12-11', 8, 0),
(12, 1, '2014-12-12', 8, 3),
(13, 1, '2014-12-13', 8, 0),
(14, 1, '2014-12-14', 8, 0),
(15, 1, '2014-12-15', 8, 0),
(16, 1, '2014-12-16', 8, 0),
(17, 1, '2014-12-17', 8, 0),
(18, 1, '2014-12-18', 8, 0),
(19, 1, '2014-12-19', 8, 0),
(20, 1, '2014-12-20', 8, 0),
(21, 1, '2014-12-21', 8, 0),
(22, 1, '2014-12-22', 8, 0),
(23, 1, '2014-12-23', 8, 0),
(24, 1, '2014-12-24', 8, 0),
(25, 1, '2014-12-25', 8, 0),
(26, 1, '2014-12-26', 8, 0),
(27, 1, '2014-12-27', 8, 0),
(28, 1, '2014-12-28', 8, 0),
(29, 1, '2014-12-29', 8, 0),
(30, 1, '2014-12-30', 8, 0),
(31, 1, '2014-12-31', 8, 0),
(32, 2, '2014-12-01', 8, 2),
(33, 2, '2014-12-02', 8, 0),
(34, 2, '2014-12-03', 8, 0),
(35, 2, '2014-12-04', 8, 0),
(36, 2, '2014-12-05', 8, 0),
(37, 2, '2014-12-06', 8, 0),
(38, 2, '2014-12-07', 8, 0),
(39, 2, '2014-12-08', 8, 0),
(40, 2, '2014-12-09', 8, 0),
(41, 2, '2014-12-10', 8, 0),
(42, 2, '2014-12-11', 8, 0),
(43, 2, '2014-12-12', 8, 0),
(44, 2, '2014-12-13', 8, 0),
(45, 2, '2014-12-14', 8, 0),
(46, 2, '2014-12-15', 8, 0),
(47, 2, '2014-12-16', 8, 0),
(48, 2, '2014-12-17', 8, 0),
(49, 2, '2014-12-18', 8, 0),
(50, 2, '2014-12-19', 8, 0),
(51, 2, '2014-12-20', 8, 0),
(52, 2, '2014-12-21', 8, 0),
(53, 2, '2014-12-22', 8, 0),
(54, 2, '2014-12-23', 8, 0),
(55, 2, '2014-12-24', 8, 0),
(56, 2, '2014-12-25', 8, 0),
(57, 2, '2014-12-26', 8, 0),
(58, 2, '2014-12-27', 8, 0),
(59, 2, '2014-12-28', 8, 0),
(60, 2, '2014-12-29', 8, 0),
(61, 2, '2014-12-30', 8, 0),
(62, 2, '2014-12-31', 8, 0),
(63, 3, '2014-12-01', 8, 8),
(64, 3, '2014-12-02', 8, 0),
(65, 3, '2014-12-03', 8, 0),
(66, 3, '2014-12-04', 8, 0),
(67, 3, '2014-12-05', 8, 0),
(68, 3, '2014-12-06', 8, 0),
(69, 3, '2014-12-07', 8, 0),
(70, 3, '2014-12-08', 8, 0),
(71, 3, '2014-12-09', 8, 0),
(72, 3, '2014-12-10', 8, 0),
(73, 3, '2014-12-11', 8, 0),
(74, 3, '2014-12-12', 8, 0),
(75, 3, '2014-12-13', 8, 0),
(76, 3, '2014-12-14', 8, 0),
(77, 3, '2014-12-15', 8, 0),
(78, 3, '2014-12-16', 8, 0),
(79, 3, '2014-12-17', 8, 0),
(80, 3, '2014-12-18', 8, 0),
(81, 3, '2014-12-19', 8, 0),
(82, 3, '2014-12-20', 8, 0),
(83, 3, '2014-12-21', 8, 0),
(84, 3, '2014-12-22', 8, 0),
(85, 3, '2014-12-23', 8, 0),
(86, 3, '2014-12-24', 8, 0),
(87, 3, '2014-12-25', 8, 0),
(88, 3, '2014-12-26', 8, 0),
(89, 3, '2014-12-27', 8, 0),
(90, 3, '2014-12-28', 8, 0),
(91, 3, '2014-12-29', 8, 0),
(92, 3, '2014-12-30', 8, 0),
(93, 3, '2014-12-31', 8, 0),
(94, 4, '2014-12-01', 8, 2),
(95, 4, '2014-12-02', 8, 0),
(96, 4, '2014-12-03', 8, 0),
(97, 4, '2014-12-04', 8, 0),
(98, 4, '2014-12-05', 8, 0),
(99, 4, '2014-12-06', 8, 0),
(100, 4, '2014-12-07', 8, 0),
(101, 4, '2014-12-08', 8, 0),
(102, 4, '2014-12-09', 8, 0),
(103, 4, '2014-12-10', 8, 0),
(104, 4, '2014-12-11', 8, 0),
(105, 4, '2014-12-12', 8, 0),
(106, 4, '2014-12-13', 8, 0),
(107, 4, '2014-12-14', 8, 0),
(108, 4, '2014-12-15', 8, 0),
(109, 4, '2014-12-16', 8, 0),
(110, 4, '2014-12-17', 8, 0),
(111, 4, '2014-12-18', 8, 0),
(112, 4, '2014-12-19', 8, 0),
(113, 4, '2014-12-20', 8, 0),
(114, 4, '2014-12-21', 8, 0),
(115, 4, '2014-12-22', 8, 0),
(116, 4, '2014-12-23', 8, 0),
(117, 4, '2014-12-24', 8, 0),
(118, 4, '2014-12-25', 8, 0),
(119, 4, '2014-12-26', 8, 0),
(120, 4, '2014-12-27', 8, 0),
(121, 4, '2014-12-28', 8, 0),
(122, 4, '2014-12-29', 8, 0),
(123, 4, '2014-12-30', 8, 0),
(124, 4, '2014-12-31', 8, 0),
(125, 5, '2014-12-01', 8, 2),
(126, 5, '2014-12-02', 8, 0),
(127, 5, '2014-12-03', 8, 0),
(128, 5, '2014-12-04', 8, 0),
(129, 5, '2014-12-05', 8, 0),
(130, 5, '2014-12-06', 8, 0),
(131, 5, '2014-12-07', 8, 0),
(132, 5, '2014-12-08', 8, 0),
(133, 5, '2014-12-09', 8, 0),
(134, 5, '2014-12-10', 8, 0),
(135, 5, '2014-12-11', 8, 0),
(136, 5, '2014-12-12', 8, 0),
(137, 5, '2014-12-13', 8, 0),
(138, 5, '2014-12-14', 8, 0),
(139, 5, '2014-12-15', 8, 0),
(140, 5, '2014-12-16', 8, 0),
(141, 5, '2014-12-17', 8, 0),
(142, 5, '2014-12-18', 8, 0),
(143, 5, '2014-12-19', 8, 0),
(144, 5, '2014-12-20', 8, 0),
(145, 5, '2014-12-21', 8, 0),
(146, 5, '2014-12-22', 8, 0),
(147, 5, '2014-12-23', 8, 0),
(148, 5, '2014-12-24', 8, 0),
(149, 5, '2014-12-25', 8, 0),
(150, 5, '2014-12-26', 8, 0),
(151, 5, '2014-12-27', 8, 0),
(152, 5, '2014-12-28', 8, 0),
(153, 5, '2014-12-29', 8, 0),
(154, 5, '2014-12-30', 8, 0),
(155, 5, '2014-12-31', 8, 0),
(156, 1, '2015-01-01', 8, 0),
(157, 1, '2015-01-02', 8, 0),
(158, 1, '2015-01-03', 8, 0),
(159, 1, '2015-01-04', 8, 0),
(160, 1, '2015-01-05', 8, 0),
(161, 1, '2015-01-06', 8, 0),
(162, 1, '2015-01-07', 8, 0),
(163, 1, '2015-01-08', 8, 0),
(164, 1, '2015-01-09', 8, 0),
(165, 1, '2015-01-10', 8, 0),
(166, 1, '2015-01-11', 8, 0),
(167, 1, '2015-01-12', 8, 0),
(168, 1, '2015-01-13', 8, 0),
(169, 1, '2015-01-14', 8, 0),
(170, 1, '2015-01-15', 8, 0),
(171, 1, '2015-01-16', 8, 0),
(172, 1, '2015-01-17', 8, 0),
(173, 1, '2015-01-18', 8, 0),
(174, 1, '2015-01-19', 8, 0),
(175, 1, '2015-01-20', 8, 0),
(176, 1, '2015-01-21', 8, 0),
(177, 1, '2015-01-22', 8, 0),
(178, 1, '2015-01-23', 8, 0),
(179, 1, '2015-01-24', 8, 0),
(180, 1, '2015-01-25', 8, 0),
(181, 1, '2015-01-26', 8, 0),
(182, 1, '2015-01-27', 8, 0),
(183, 1, '2015-01-28', 8, 0),
(184, 1, '2015-01-29', 8, 0),
(185, 1, '2015-01-30', 8, 0),
(186, 1, '2015-01-31', 8, 0),
(187, 2, '2015-01-01', 8, 0),
(188, 2, '2015-01-02', 8, 0),
(189, 2, '2015-01-03', 0, 0),
(190, 2, '2015-01-04', 8, 0),
(191, 2, '2015-01-05', 8, 0),
(192, 2, '2015-01-06', 8, 0),
(193, 2, '2015-01-07', 8, 0),
(194, 2, '2015-01-08', 8, 0),
(195, 2, '2015-01-09', 8, 0),
(196, 2, '2015-01-10', 0, 0),
(197, 2, '2015-01-11', 8, 0),
(198, 2, '2015-01-12', 8, 0),
(199, 2, '2015-01-13', 8, 0),
(200, 2, '2015-01-14', 8, 0),
(201, 2, '2015-01-15', 8, 0),
(202, 2, '2015-01-16', 8, 0),
(203, 2, '2015-01-17', 0, 0),
(204, 2, '2015-01-18', 8, 0),
(205, 2, '2015-01-19', 4, 0),
(206, 2, '2015-01-20', 8, 0),
(207, 2, '2015-01-21', 8, 0),
(208, 2, '2015-01-22', 8, 0),
(209, 2, '2015-01-23', 8, 0),
(210, 2, '2015-01-24', 0, 0),
(211, 2, '2015-01-25', 8, 0),
(212, 2, '2015-01-26', 8, 0),
(213, 2, '2015-01-27', 8, 0),
(214, 2, '2015-01-28', 8, 0),
(215, 2, '2015-01-29', 8, 0),
(216, 2, '2015-01-30', 8, 0),
(217, 2, '2015-01-31', 0, 0),
(218, 3, '2015-01-01', 8, 0),
(219, 3, '2015-01-02', 8, 0),
(220, 3, '2015-01-03', 0, 0),
(221, 3, '2015-01-04', 8, 0),
(222, 3, '2015-01-05', 0, 0),
(223, 3, '2015-01-06', 8, 0),
(224, 3, '2015-01-07', 8, 0),
(225, 3, '2015-01-08', 8, 0),
(226, 3, '2015-01-09', 8, 0),
(227, 3, '2015-01-10', 0, 0),
(228, 3, '2015-01-11', 8, 0),
(229, 3, '2015-01-12', 0, 0),
(230, 3, '2015-01-13', 8, 0),
(231, 3, '2015-01-14', 8, 0),
(232, 3, '2015-01-15', 4, 0),
(233, 3, '2015-01-16', 8, 0),
(234, 3, '2015-01-17', 0, 0),
(235, 3, '2015-01-18', 8, 0),
(236, 3, '2015-01-19', 8, 0),
(237, 3, '2015-01-20', 8, 0),
(238, 3, '2015-01-21', 8, 0),
(239, 3, '2015-01-22', 8, 0),
(240, 3, '2015-01-23', 8, 0),
(241, 3, '2015-01-24', 0, 0),
(242, 3, '2015-01-25', 8, 0),
(243, 3, '2015-01-26', 8, 0),
(244, 3, '2015-01-27', 8, 0),
(245, 3, '2015-01-28', 8, 0),
(246, 3, '2015-01-29', 8, 0),
(247, 3, '2015-01-30', 8, 0),
(248, 3, '2015-01-31', 0, 0),
(249, 4, '2015-01-01', 8, 0),
(250, 4, '2015-01-02', 8, 0),
(251, 4, '2015-01-03', 0, 0),
(252, 4, '2015-01-04', 8, 0),
(253, 4, '2015-01-05', 8, 0),
(254, 4, '2015-01-06', 8, 0),
(255, 4, '2015-01-07', 8, 0),
(256, 4, '2015-01-08', 8, 0),
(257, 4, '2015-01-09', 8, 0),
(258, 4, '2015-01-10', 0, 0),
(259, 4, '2015-01-11', 8, 0),
(260, 4, '2015-01-12', 8, 0),
(261, 4, '2015-01-13', 8, 0),
(262, 4, '2015-01-14', 8, 0),
(263, 4, '2015-01-15', 8, 0),
(264, 4, '2015-01-16', 8, 0),
(265, 4, '2015-01-17', 0, 0),
(266, 4, '2015-01-18', 8, 0),
(267, 4, '2015-01-19', 8, 0),
(268, 4, '2015-01-20', 8, 0),
(269, 4, '2015-01-21', 8, 0),
(270, 4, '2015-01-22', 8, 0),
(271, 4, '2015-01-23', 8, 0),
(272, 4, '2015-01-24', 0, 0),
(273, 4, '2015-01-25', 4, 0),
(274, 4, '2015-01-26', 8, 0),
(275, 4, '2015-01-27', 8, 0),
(276, 4, '2015-01-28', 8, 0),
(277, 4, '2015-01-29', 8, 0),
(278, 4, '2015-01-30', 8, 0),
(279, 4, '2015-01-31', 0, 0),
(280, 5, '2015-01-01', 8, 0),
(281, 5, '2015-01-02', 8, 0),
(282, 5, '2015-01-03', 0, 0),
(283, 5, '2015-01-04', 8, 0),
(284, 5, '2015-01-05', 8, 0),
(285, 5, '2015-01-06', 8, 0),
(286, 5, '2015-01-07', 8, 0),
(287, 5, '2015-01-08', 8, 0),
(288, 5, '2015-01-09', 8, 0),
(289, 5, '2015-01-10', 0, 0),
(290, 5, '2015-01-11', 8, 0),
(291, 5, '2015-01-12', 8, 0),
(292, 5, '2015-01-13', 8, 0),
(293, 5, '2015-01-14', 8, 0),
(294, 5, '2015-01-15', 8, 0),
(295, 5, '2015-01-16', 8, 0),
(296, 5, '2015-01-17', 0, 0),
(297, 5, '2015-01-18', 8, 0),
(298, 5, '2015-01-19', 8, 0),
(299, 5, '2015-01-20', 8, 0),
(300, 5, '2015-01-21', 8, 0),
(301, 5, '2015-01-22', 8, 0),
(302, 5, '2015-01-23', 8, 0),
(303, 5, '2015-01-24', 0, 0),
(304, 5, '2015-01-25', 8, 0),
(305, 5, '2015-01-26', 8, 0),
(306, 5, '2015-01-27', 8, 0),
(307, 5, '2015-01-28', 8, 0),
(308, 5, '2015-01-29', 8, 0),
(309, 5, '2015-01-30', 8, 0),
(310, 5, '2015-01-31', 0, 0),
(311, 1, '2015-02-01', 8, 3),
(312, 1, '2015-02-02', 8, 0),
(313, 1, '2015-02-03', 8, 0),
(314, 1, '2015-02-04', 8, 0),
(315, 1, '2015-02-05', 8, 0),
(316, 1, '2015-02-06', 8, 0),
(317, 1, '2015-02-07', 8, 0),
(318, 1, '2015-02-08', 8, 0),
(319, 1, '2015-02-09', 8, 0),
(320, 1, '2015-02-10', 8, 0),
(321, 1, '2015-02-11', 8, 0),
(322, 1, '2015-02-12', 8, 0),
(323, 1, '2015-02-13', 8, 0),
(324, 1, '2015-02-14', 8, 0),
(325, 1, '2015-02-15', 8, 0),
(326, 1, '2015-02-16', 8, 0),
(327, 1, '2015-02-17', 8, 0),
(328, 1, '2015-02-18', 8, 0),
(329, 1, '2015-02-19', 8, 0),
(330, 1, '2015-02-20', 8, 0),
(331, 1, '2015-02-21', 8, 0),
(332, 1, '2015-02-22', 8, 0),
(333, 1, '2015-02-23', 8, 0),
(334, 1, '2015-02-24', 8, 0),
(335, 1, '2015-02-25', 8, 0),
(336, 1, '2015-02-26', 8, 0),
(337, 1, '2015-02-27', 8, 0),
(338, 1, '2015-02-28', 8, 0),
(339, 2, '2015-02-01', 8, 0),
(340, 2, '2015-02-02', 8, 0),
(341, 2, '2015-02-03', 8, 0),
(342, 2, '2015-02-04', 8, 0),
(343, 2, '2015-02-05', 8, 0),
(344, 2, '2015-02-06', 8, 0),
(345, 2, '2015-02-07', 8, 0),
(346, 2, '2015-02-08', 8, 0),
(347, 2, '2015-02-09', 8, 0),
(348, 2, '2015-02-10', 8, 0),
(349, 2, '2015-02-11', 8, 0),
(350, 2, '2015-02-12', 8, 0),
(351, 2, '2015-02-13', 8, 0),
(352, 2, '2015-02-14', 8, 0),
(353, 2, '2015-02-15', 8, 0),
(354, 2, '2015-02-16', 8, 0),
(355, 2, '2015-02-17', 8, 0),
(356, 2, '2015-02-18', 8, 0),
(357, 2, '2015-02-19', 8, 0),
(358, 2, '2015-02-20', 8, 0),
(359, 2, '2015-02-21', 8, 0),
(360, 2, '2015-02-22', 8, 0),
(361, 2, '2015-02-23', 8, 0),
(362, 2, '2015-02-24', 8, 0),
(363, 2, '2015-02-25', 8, 0),
(364, 2, '2015-02-26', 8, 0),
(365, 2, '2015-02-27', 8, 0),
(366, 2, '2015-02-28', 8, 0),
(367, 3, '2015-02-01', 8, 0),
(368, 3, '2015-02-02', 8, 0),
(369, 3, '2015-02-03', 8, 0),
(370, 3, '2015-02-04', 8, 0),
(371, 3, '2015-02-05', 8, 0),
(372, 3, '2015-02-06', 8, 0),
(373, 3, '2015-02-07', 8, 0),
(374, 3, '2015-02-08', 8, 0),
(375, 3, '2015-02-09', 8, 0),
(376, 3, '2015-02-10', 8, 0),
(377, 3, '2015-02-11', 8, 0),
(378, 3, '2015-02-12', 8, 0),
(379, 3, '2015-02-13', 8, 0),
(380, 3, '2015-02-14', 8, 0),
(381, 3, '2015-02-15', 8, 0),
(382, 3, '2015-02-16', 8, 0),
(383, 3, '2015-02-17', 8, 0),
(384, 3, '2015-02-18', 8, 0),
(385, 3, '2015-02-19', 8, 0),
(386, 3, '2015-02-20', 8, 0),
(387, 3, '2015-02-21', 8, 0),
(388, 3, '2015-02-22', 8, 0),
(389, 3, '2015-02-23', 8, 0),
(390, 3, '2015-02-24', 8, 0),
(391, 3, '2015-02-25', 8, 0),
(392, 3, '2015-02-26', 8, 0),
(393, 3, '2015-02-27', 8, 0),
(394, 3, '2015-02-28', 8, 0),
(395, 4, '2015-02-01', 8, 0),
(396, 4, '2015-02-02', 8, 0),
(397, 4, '2015-02-03', 8, 0),
(398, 4, '2015-02-04', 8, 0),
(399, 4, '2015-02-05', 8, 0),
(400, 4, '2015-02-06', 8, 0),
(401, 4, '2015-02-07', 8, 0),
(402, 4, '2015-02-08', 8, 0),
(403, 4, '2015-02-09', 8, 0),
(404, 4, '2015-02-10', 8, 0),
(405, 4, '2015-02-11', 8, 0),
(406, 4, '2015-02-12', 8, 0),
(407, 4, '2015-02-13', 8, 0),
(408, 4, '2015-02-14', 8, 0),
(409, 4, '2015-02-15', 8, 0),
(410, 4, '2015-02-16', 8, 0),
(411, 4, '2015-02-17', 8, 0),
(412, 4, '2015-02-18', 8, 0),
(413, 4, '2015-02-19', 8, 0),
(414, 4, '2015-02-20', 8, 0),
(415, 4, '2015-02-21', 8, 0),
(416, 4, '2015-02-22', 8, 0),
(417, 4, '2015-02-23', 8, 0),
(418, 4, '2015-02-24', 8, 0),
(419, 4, '2015-02-25', 8, 0),
(420, 4, '2015-02-26', 8, 0),
(421, 4, '2015-02-27', 8, 0),
(422, 4, '2015-02-28', 8, 0),
(423, 5, '2015-02-01', 8, 0),
(424, 5, '2015-02-02', 8, 0),
(425, 5, '2015-02-03', 8, 0),
(426, 5, '2015-02-04', 8, 0),
(427, 5, '2015-02-05', 8, 0),
(428, 5, '2015-02-06', 8, 0),
(429, 5, '2015-02-07', 8, 0),
(430, 5, '2015-02-08', 8, 0),
(431, 5, '2015-02-09', 8, 0),
(432, 5, '2015-02-10', 8, 0),
(433, 5, '2015-02-11', 8, 0),
(434, 5, '2015-02-12', 8, 0),
(435, 5, '2015-02-13', 8, 0),
(436, 5, '2015-02-14', 8, 0),
(437, 5, '2015-02-15', 8, 0),
(438, 5, '2015-02-16', 8, 0),
(439, 5, '2015-02-17', 8, 0),
(440, 5, '2015-02-18', 8, 0),
(441, 5, '2015-02-19', 8, 0),
(442, 5, '2015-02-20', 8, 0),
(443, 5, '2015-02-21', 8, 0),
(444, 5, '2015-02-22', 8, 0),
(445, 5, '2015-02-23', 8, 0),
(446, 5, '2015-02-24', 8, 0),
(447, 5, '2015-02-25', 8, 0),
(448, 5, '2015-02-26', 8, 0),
(449, 5, '2015-02-27', 8, 0),
(450, 5, '2015-02-28', 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bonus_fine`
--

CREATE TABLE `bonus_fine` (
  `bnf_id` int(11) NOT NULL AUTO_INCREMENT,
  `bnf_staff_id` int(11) DEFAULT NULL,
  `bnf_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bnf_date` date DEFAULT NULL,
  `bnf_about` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bnf_amount` float DEFAULT NULL,
  PRIMARY KEY (`bnf_id`),
  UNIQUE KEY `bnf_id` (`bnf_id`),
  KEY `bnf_staff_id_2` (`bnf_staff_id`),
  KEY `bnf_staff_id_3` (`bnf_staff_id`),
  KEY `bnf_staff_id_4` (`bnf_staff_id`),
  KEY `bnf_staff_id_5` (`bnf_staff_id`),
  KEY `bnf_staff_id_6` (`bnf_staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `bonus_fine`
--

INSERT INTO `bonus_fine` (`bnf_id`, `bnf_staff_id`, `bnf_type`, `bnf_date`, `bnf_about`, `bnf_amount`) VALUES
(2, 2, '0', '2014-12-28', 'Phạt chậm tiến độ dự án Lorem', 500000),
(8, 1, '1', '2014-12-29', NULL, 120000),
(9, 4, '1', '2014-12-29', 'Lam viec nhu lz', 120000),
(10, 3, '0', '2014-12-29', 'Ghét', 120000);

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE `bulletin` (
  `pst_id` int(11) NOT NULL AUTO_INCREMENT,
  `pst_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `pst_time` datetime NOT NULL,
  `pst_content` text COLLATE utf8_unicode_ci NOT NULL,
  `pst_is_recruitment` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pst_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `bulletin`
--

INSERT INTO `bulletin` (`pst_id`, `pst_title`, `pst_time`, `pst_content`, `pst_is_recruitment`) VALUES
(9, 'Thông báo trợ cấp hộ nghèo', '2014-12-10 09:19:56', 'Những nhân viên nào thuộc diện hộ nghèo, mang giấy chứng nhận hộ nghèo lên phòng nhân sự để được hưởng trợ cấp.<br><b>Khi đi nhớ mang theo CMND</b>', 0),
(10, 'Thông báo về việc lấy thông tin nhân viên', '2014-12-18 09:18:05', 'Hiện nay còn một số nhân viên chưa cung cấp đủ thông tin sơ yếu lý lịch.<br>Những nhân viên nào có tên sau đề nghị cuối giờ chiều ngày 28/12/2015 nộp lại sơ yếu lý lịch.<br><b>Nguyễn Viết Toàn</b><br><b>Lê Tấn Trường<br></b><br><i>Khi đi nhớ mang theo CMND</i><br><br>', 0),
(11, 'Thông báo nhận lương tháng 12', '2014-12-18 09:15:21', 'Ngày 25/12/2015, công ty sẽ trả lương tháng 12. Đề nghị mọi người đến phòng tài vụ để lĩnh.<br>', 0),
(12, 'Thông báo nghỉ tết dương 2015', '2014-12-25 09:11:44', 'Toàn công ty&nbsp;sẽ nghỉ Tết trong 04 ngày từ thứ Năm, ngày 01/01/2015 đến hết Chủ nhật, ngày 04/01/2015. Thứ Hai, ngày 05/01/2015 làm việc bình thường.<br>Thứ Bảy, ngày 27/12/2014 toàn công ty&nbsp;làm việc bù cho thứ Sáu, ngày 02/01/2015.', 0),
(13, 'Tuyển dụng tháng 1 năm 2015', '2015-01-14 08:28:31', '<b>Cần tuyển:</b><br>1 thợ tiện<br>1 thợ hàn<br><br><b>Yêu cầu:</b><br>Sức khỏe tốt<br>Không tiền án tiền sự<br><br><b>Chi tiết liên hệ:</b><br>0912123456', 1);

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE `candidate` (
  `cdd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cdd_fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdd_province_id` int(11) NOT NULL,
  `cdd_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdd_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdd_degree` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdd_skill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdd_expected_salary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdd_is_approved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cdd_id`),
  KEY `cdd_hometown` (`cdd_province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`cdd_id`, `cdd_fullname`, `cdd_province_id`, `cdd_email`, `cdd_phone`, `cdd_degree`, `cdd_skill`, `cdd_expected_salary`, `cdd_is_approved`) VALUES
(5, 'Olala Alolo', 4, 'example@email.com', '123456678', 'Dai hoc', 'Chem gio', '5000000', 1),
(6, 'Olala Alolo', 4, 'example@email.com', '123456678', 'Dai hoc', 'Chem gio', '5000000', 1),
(7, '123123213', 1, '123123', '213123', '123123', '123123', '123123', 0),
(11, '', 1, '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dep_id` int(11) NOT NULL AUTO_INCREMENT,
  `dep_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`dep_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dep_id`, `dep_name`) VALUES
(1, 'Tài vụ'),
(2, 'Kỹ thuật');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `eve_id` int(11) NOT NULL AUTO_INCREMENT,
  `eve_date` date NOT NULL,
  `eve_place` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `eve_content` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`eve_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`eve_id`, `eve_date`, `eve_place`, `eve_content`) VALUES
(1, '2012-12-31', 'Hanoi', 'Aloha'),
(2, '2012-12-31', 'Hanoi', 'Bloha');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `psn_id` int(11) NOT NULL AUTO_INCREMENT,
  `psn_department_id` int(11) NOT NULL,
  `psn_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`psn_id`),
  KEY `psn_department_id` (`psn_department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`psn_id`, `psn_department_id`, `psn_name`) VALUES
(1, 2, 'Thợ tiện'),
(2, 2, 'Thợ hàn'),
(5, 2, 'Thợ nguội'),
(6, 1, 'Kế toán');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `prv_id` int(11) NOT NULL AUTO_INCREMENT,
  `prv_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`prv_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=64 ;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`prv_id`, `prv_name`) VALUES
(1, 'An Giang'),
(2, 'Bà Rịa - Vũng Tàu'),
(3, 'Bình Dương'),
(4, 'Bình Phước'),
(5, 'Bình Thuận'),
(6, 'Bình Định'),
(7, 'Bạc Liêu'),
(8, 'Bắc Giang'),
(9, 'Bắc Kạn'),
(10, 'Bắc Ninh'),
(11, 'Bến Tre'),
(12, 'Cao Bằng'),
(13, 'Cà Mau'),
(14, 'Cần Thơ'),
(15, 'Điện Biên'),
(16, 'Đà Nẵng'),
(17, 'Đắk Lắk'),
(18, 'Đắk Nông'),
(19, 'Đồng Nai'),
(20, 'Đồng Tháp'),
(21, 'Gia Lai'),
(22, 'Hà Giang'),
(23, 'Hà Nam'),
(24, 'Hà Nội'),
(25, 'Hà Tĩnh'),
(26, 'Hòa Bình'),
(27, 'Hưng Yên'),
(28, 'Hải Dương'),
(29, 'Hải Phòng'),
(30, 'Hậu Giang'),
(31, 'Khánh Hòa'),
(32, 'Kiên Giang'),
(33, 'Kon Tum'),
(34, 'Lai Châu'),
(35, 'Long An'),
(36, 'Lào Cai'),
(37, 'Lâm Đồng'),
(38, 'Lạng Sơn'),
(39, 'Nam Định'),
(40, 'Nghệ An'),
(41, 'Ninh Bình'),
(42, 'Ninh Thuận'),
(43, 'Phú Thọ'),
(44, 'Phú Yên'),
(45, 'Quảng Bình'),
(46, 'Quảng Nam'),
(47, 'Quảng Ngãi'),
(48, 'Quảng Ninh'),
(49, 'Quảng Trị'),
(50, 'Sóc Trăng'),
(51, 'Sơn La'),
(52, 'Thanh Hóa'),
(53, 'Thái Bình'),
(54, 'Thái Nguyên'),
(55, 'Thừa Thiên Huế'),
(56, 'Tiền Giang'),
(57, 'TP HCM'),
(58, 'Trà Vinh'),
(59, 'Tuyên Quang'),
(60, 'Tây Ninh'),
(61, 'Vĩnh Long'),
(62, 'Vĩnh Phúc'),
(63, 'Yên Bái');

-- --------------------------------------------------------

--
-- Table structure for table `salary_config`
--

CREATE TABLE `salary_config` (
  `slc_id` int(11) NOT NULL AUTO_INCREMENT,
  `slc_standard_daywork_hours` int(11) NOT NULL,
  `slc_standard_points` float NOT NULL,
  `slc_attendance_bonus` float NOT NULL,
  `slc_extra_point_rate` float NOT NULL,
  PRIMARY KEY (`slc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `salary_config`
--

INSERT INTO `salary_config` (`slc_id`, `slc_standard_daywork_hours`, `slc_standard_points`, `slc_attendance_bonus`, `slc_extra_point_rate`) VALUES
(1, 8, 26, 500000, 20);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `sta_id` int(11) NOT NULL AUTO_INCREMENT,
  `sta_username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sta_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `sta_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sta_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sta_civilianId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sta_dob` date NOT NULL,
  `sta_hometown` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sta_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sta_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sta_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sta_permission` int(11) NOT NULL,
  `sta_position_id` int(11) NOT NULL,
  `sta_single_attendance_amount` float NOT NULL,
  `sta_contract_deadline` date NOT NULL,
  PRIMARY KEY (`sta_id`),
  KEY `sta_position_id` (`sta_position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`sta_id`, `sta_username`, `sta_password`, `sta_firstname`, `sta_lastname`, `sta_civilianId`, `sta_dob`, `sta_hometown`, `sta_address`, `sta_email`, `sta_phone`, `sta_permission`, `sta_position_id`, `sta_single_attendance_amount`, `sta_contract_deadline`) VALUES
(1, '02010001', '2e3bfa637e72507d70dbc2112914ed34', 'Bùi Gia', 'Thịnh', '412312123213', '1993-09-10', 'Hanoi', 'Gia Lam', 'nguyetanh@email.com', '0987654321', 2, 1, 150000, '2014-11-30'),
(2, '02010002', '2e3bfa637e72507d70dbc2112914ed34', 'Lê Hương', 'Linh', '165382098', '1988-09-10', 'Thái Bình', 'Thái Bình', 'example@email.com', '0987654321', 1, 6, 190000, '2015-12-31'),
(3, '02010003', '11ba88e2a1ff73b56d189591515d528c', 'Nguyễn Khánh', 'Toàn', '123456789', '1993-01-01', 'Hanoi', 'Hoan Kiem', 'quan@email.com', '0987654321', 0, 1, 150000, '2014-11-30'),
(4, '02010004', '2b8395095cc802e1dbceeb9bd47d3598', 'Lê Tấn', 'Trường', '123456789', '1993-01-01', 'Hanoi', 'Gia Lam', 'quyen@email.com', '0987654321', 0, 1, 150000, '2014-11-30'),
(5, '02010005', '67c5e985257dd7b3e3bff4ce44fd683f', 'Nguyễn Vĩnh', 'Long', '123123', '2011-11-11', 'USA', 'New York', 'taylor.swift@haintheme.com', '12588943523', 0, 1, 150000, '2015-11-11');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `absence_request`
--
ALTER TABLE `absence_request`
  ADD CONSTRAINT `fk_absence_request_staff1` FOREIGN KEY (`abr_staff_id`) REFERENCES `staff` (`sta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `advance_request`
--
ALTER TABLE `advance_request`
  ADD CONSTRAINT `fk_advance_request_staff1` FOREIGN KEY (`adr_staff_id`) REFERENCES `staff` (`sta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`att_staff_id`) REFERENCES `staff` (`sta_id`);

--
-- Constraints for table `bonus_fine`
--
ALTER TABLE `bonus_fine`
  ADD CONSTRAINT `bonus_fine_ibfk_1` FOREIGN KEY (`bnf_staff_id`) REFERENCES `staff` (`sta_id`);

--
-- Constraints for table `candidate`
--
ALTER TABLE `candidate`
  ADD CONSTRAINT `candidate_ibfk_1` FOREIGN KEY (`cdd_province_id`) REFERENCES `province` (`prv_id`);

--
-- Constraints for table `position`
--
ALTER TABLE `position`
  ADD CONSTRAINT `position_ibfk_1` FOREIGN KEY (`psn_department_id`) REFERENCES `department` (`dep_id`);

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`sta_position_id`) REFERENCES `position` (`psn_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
