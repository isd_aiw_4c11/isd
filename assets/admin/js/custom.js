(function( $ ) {
	var departmentPositionSelect = function() {
		$('#select-department').on('change', function(){
			getUrl = $(this).data('url') +'/'+ $(this).val();
			$.ajax({
				url: getUrl,
				type: 'GET',
				dataType: 'html',
			})
			.done(function(data) {
				$('#select-position')
					.prop("disabled", false)
					.html(data);
			});
			
		})
	}
	$(document).ready(function() {

		departmentPositionSelect();

		$('#select-department').trigger('change');

		$('table.data-table').each(function(){
			if ( $(this).hasClass('desc') ) {
				var orderPos = $(this).data('order-column');
				$(this).dataTable({
					"order": [[ orderPos, "desc" ]]
				});
			} else {
				$(this).dataTable();
			}
		})
		$('[data-mask="date"]').inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});

		$(".textarea").wysihtml5();

		$(".btn-print").printPage({
			message: 'Đang in báo cáo ...'
		});
	})
})( jQuery ); // EOF