<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-7 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Danh sách phòng ban</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered" id="department-table">
					<thead>
						<tr>
							<th>Phòng ban</th>
							<th>Vị trí</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($departments as $key => $department): ?>
							<tr>
								<td style="vertical-align: middle">
									<?php echo $department['dep_name']; ?> 
									<a onclick="return confirm('Xóa phòng ban sẽ xóa toàn bộ VỊ TRÍ và NHÂN VIÊN thuộc vị trí đó, bạn chắc chắn muốn xóa?')" class="btn btn-danger btn-flat btn-xs" href="<?php echo base_url('department/delete_department/'.$department['dep_id']); ?>">Xóa</a>
								</td>
								<td style="vertical-align: middle">
									<?php foreach ($positions as $key => $position): ?>
										<?php if ($position['psn_department_id'] == $department['dep_id']): ?>
												<p>
													<?php echo $position['psn_name']; ?> 
													<a onclick="return confirm('Xóa vị trí sẽ xóa toàn bộ NHÂN VIÊN thuộc vị trí đó, bạn chắc chắn muốn xóa?')" class="btn btn-danger btn-flat btn-xs" href="<?php echo base_url('department/delete_position/'.$position['psn_id']); ?>">Xóa</a>
												</p>
										<?php endif ?>
									<?php endforeach ?>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	<div class="col-sm-5 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Thêm phòng ban</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form method="post" action="<?php echo base_url('department/add_department') ?>">
					<div class="form-group">
						<label for="">Nhập tên phòng</label>
						<input class="form-control" type="text" name="dep[dep_name]">
					</div>
					<button type="submit" class="btn btn-info">Tạo phòng ban</button>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>

		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Thêm vị trí</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form method="post" action="<?php echo base_url('department/add_position') ?>">
					<div class="form-group">
						<label for="">Chọn phòng</label>
						<select name="psn[psn_department_id]" id="" class="form-control">
							<?php foreach ($departments as $key => $department): ?>
								<option value="<?php echo $department['dep_id']; ?>"><?php echo $department['dep_name']; ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="form-group">
						<label for="">Nhập tên vị trí</label>
						<input class="form-control" type="text" name="psn[psn_name]">
					</div>
					<button type="submit" class="btn btn-info">Tạo vị trí</button>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
