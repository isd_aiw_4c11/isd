<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">

	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title" role="alert">
					Ngày <span class="label label-info" style="font-size: 16px"><?php echo $day ?></span> Tháng <span class="label label-info" style="font-size: 16px"><?php echo $month ?></span> Năm <span class="label label-info" style="font-size: 16px"><?php echo $year ?></span>
				</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form role="form" action="<?php echo base_url('attendance/check_attendance_exe') ?>" method="POST">
					<input type="hidden" name="day" value="<?php echo $day ?>" />
					<input type="hidden" name="month" value="<?php echo $month ?>" />
					<input type="hidden" name="year" value="<?php echo $year ?>" />
					<div class="box box-solid bg-yellow">
						<div class="box-body">
							<h3 style="display: inline-block">Chú ý !</h3>. Nhập số GIỜ công vào ô, hệ thống sẽ tính số công
						</div><!-- /.box-body -->
					</div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Tên</th>
								<th>Giờ công thường</th>
								<th>Giờ công ngoài giờ</th>
							</tr>
						</thead>
					<?php
						foreach ($staffsAndPoints as $key => $staffAndPoint) {
							if ($staffAndPoint['sta_permission'] < 2) {
					?>
						<tr>
							<td>
								<label for="" class="col-sm-1 control-label"><?php echo $staffAndPoint['sta_lastname'] ?></label>
							</td>
							<td>
								<input type="text" class="form-control" placeholder="Nhập điểm công" name="staffs[<?php echo $staffAndPoint['sta_id'] ?>][att_point] ?>]" value="<?php echo $staffAndPoint['att_point'] ?>" autocomplete="off">
							</td>
							<td>
								<input type="text" class="form-control" placeholder="Nhập điểm công ngoài giờ" name="staffs[<?php echo $staffAndPoint['sta_id'] ?>][att_extra_point] ?>]" value="<?php echo $staffAndPoint['att_extra_point'] ?>" autocomplete="off">
							</td>
						</tr>
					<?php
							}
						}
					?>
					</table>
					<br/>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Lưu</button>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
