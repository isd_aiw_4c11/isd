<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">

	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Chọn tháng và năm</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form role="form" action="<?php echo base_url('attendance/add_attendance_table_step_2') ?>" method="POST">
					<div class="form-inline">
						<div class="form-group">
							<select name="month" id="" class="form-control" required>
								<option disabled selected>- chọn tháng -</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
							</select>
						</div>
						<div class="form-group">
							<select name="year" id="" class="form-control" required>
								<option disabled selected>- chọn năm -</option>
								<option value="2014">2014</option>
								<option value="2015">2015</option>
								<option value="2016">2016</option>
							</select>
						</div>
						<button type="submit" class="btn btn-success">Tiếp theo &rarr;</button>
					</div>
				</form>
			</div><!-- /.box-body -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
