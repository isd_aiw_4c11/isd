<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">

	<div class="col-lg-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title" role="alert">
					Tháng <span class="label label-info" style="font-size: 16px"><?php echo $month ?></span> Năm <span class="label label-info" style="font-size: 16px"><?php echo $year ?></span>
				</h3>
			</div><!-- /.box-header -->
			<div class="box-body no-padding">
				<?php
					// If no attendance record was found, raise error
					if ( empty($attendances) ) {
				?>
				<div class="alert alert-danger" role="alert" style="margin-left: 10px; margin-right: 10px;">
					Hiện chưa có bảng chấm công cho tháng này. <a class="alert-link" href="<?php echo base_url('attendance/add_attendance_table') ?>">Tạo ngay</a>
				</div>
				<?php
					} else {
				?>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Mã số</th>
								<th>Tên</th>
								<?php 
									for ($i=1; $i<=$daysInMonth[$month]; $i++) {
								?>
								<th><a href="<?php echo base_url() ?>attendance/check_attendance/<?php echo $i.'/'.$month.'/'.$year ?>" title="Chấm công ngày <?php echo $i ?> tháng <?php echo $month ?> năm <?php echo $year ?>"><?php echo $i ?></a></th>
								<?php
									}
								?>
							</tr>
						</thead>

						<?php
							foreach($staffs as $key => $staff) {
								if ($staff['sta_permission'] < 2) {
									echo '<tr>';
										echo '<td>'.$staff['sta_id'].'</td>';
										echo '<td>'.$staff['sta_lastname'].'</td>';
										foreach ($attendances as $key => $attendance) {
											$day = date( 'd', strtotime($attendance['att_date']) );
											if( $attendance['att_staff_id'] == $staff['sta_id']) {
												$style='';
												if ($attendance['att_point'] == 0) {
													$style='color: #aaa';
												} else {
													$style='font-weight: 700';
												}
												echo '<td style="'.$style.'">'.($attendance['att_point']/$configs[0]['slc_standard_daywork_hours']).'</td>';
											}
										}
									echo '</tr>';
								}
							}
						?>
					</table>
				</div>
				<?php
					}
				?>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<form class="form-inline" method="post" action="<?php echo base_url('attendance/view_attendance') ?>">
					<div class="form-group">
						<select name="month" id="" class="form-control" required>
							<?php
								for ($i = 1; $i <= 12; $i++) {
									if ($month == $i) {
										echo '<option value="'.str_pad($i, 2, '0', STR_PAD_LEFT).'" selected>'.str_pad($i, 2, '0', STR_PAD_LEFT).'</option>';
									} else {
										echo '<option value="'.str_pad($i, 2, '0', STR_PAD_LEFT).'">'.str_pad($i, 2, '0', STR_PAD_LEFT).'</option>';
									}
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<select name="year" id="" class="form-control" required>
							<?php
								for ($i = (int)date('Y'); $i >= (int)date('Y') - 3; $i--) {
									if ($year == $i) {
										echo '<option value="'.$i.'" selected>'.$i.'</option>';
									} else {
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
								}
							?>
						</select>
					</div>
					<button type="submit" class="btn btn-success">Đi &rarr;</button>
				</form>
				<br />
				<a href="<?php echo base_url('attendance/view_attendance/'.$month.'/'.$year); ?>" class="btn btn-info btn-print"><i class="fa fa-print"></i> In trang này</a>
			</div>
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
