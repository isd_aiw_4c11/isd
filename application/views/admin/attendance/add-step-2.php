<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">

	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Chọn nhân viên để tạo bảng chấm công</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<?php
					if ( empty( $availableStaffs ) ) {
						echo '<div class="alert alert-info" role="alert"><strong><i class="fa fa-info-circle"></i></strong> Tất cả nhân viên đã được tạo bảng chấm công cho tháng năm bạn vừa chọn</div>';
					} else {
				?>
					<form role="form" action="<?php echo base_url('attendance/add_attendance_table_exe') ?>" method="POST">
						<input type="hidden" value="<?php echo $month ?>" name="month">
						<input type="hidden" value="<?php echo $year ?>" name="year">
						<div class="form-group">
							<?php 
								foreach ($availableStaffs as $key => $staff) {
									if ($staff['sta_permission'] < 2) {
							?>
							<label class="checkbox-inline">
								<input type="checkbox" value="<?php echo $staff['sta_id']; ?>" name="staffs[]"> <?php echo $staff['sta_lastname'] ?>
							</label>
							<?php
									}
								}
							?>
						</div>
						<button type="submit" class="btn btn-success">Tạo &rarr;</button>
					</form>
				<?php
					}
				?>
			</div><!-- /.box-body -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->