<!DOCTYPE html>
<html class="bg-black">
	<head>
		<meta charset="UTF-8">
		<title>AdminLTE | Log in</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- bootstrap 3.0.2 -->
		<?php echo link_tag('assets/admin/css/bootstrap.min.css'); ?>
		<!-- font Awesome -->
		<?php echo link_tag('assets/admin/css/font-awesome.min.css'); ?>
		<!-- Theme style -->
		<?php echo link_tag('assets/admin/css/AdminLTE.css'); ?>

	</head>
	<body class="bg-black">

		<div class="form-box" id="login-box">
			<div class="header">Đăng nhập</div>
			<form action="<?php echo base_url('staff/authen') ?>" method="POST">
				<div class="body bg-gray">
					<div class="form-group">
						<input type="text" name="username" class="form-control" placeholder="Tài khoản"/>
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Mật khẩu"/>
					</div>          
				</div>
				<div class="footer">                                                               
					<button type="submit" class="btn bg-olive btn-block">Đăng nhập ngay</button>  
				</div>
			</form>
		</div>     

	</body>
</html>