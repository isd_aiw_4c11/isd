<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-8 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Tin tuyển dụng</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<a href="<?php echo base_url('recruitment/create_news') ?>" class="btn btn-success">Tạo tin mới</a>
				<table class="table">
					<thead>
						<tr>
							<th>Tiêu đề</th>
							<th>Ngày đăng</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($newss as $key => $news): ?>
							<tr>
								<td><?php echo $news['pst_title'] ?></td>
								<td><?php echo $news['pst_time'] ?></td>
								<td>
									<a href="<?php echo base_url('recruitment/edit_news/'.$news['pst_id']) ?>" class="btn btn-xs btn-info">Sửa</a>
									<a onclick="confirm('Bạn chắc chắn muốn xóa?');" href="<?php echo base_url('recruitment/remove_news/'.$news['pst_id']) ?>" class="btn btn-xs btn-danger">Xóa</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->