<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Danh sách ứng tuyển</h3>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<table class="table data-table desc" data-order-column="0">
					<thead>
						<tr>
							<th>Mã số</th>
							<th>Họ & tên</th>
							<th>Tỉnh</th>
							<th>Email</th>
							<th>Điện thoại</th>
							<th>Bằng cấp</th>
							<th>Sở trường / Kỹ năng</th>
							<th>Mức lương mong muốn</th>
							<th>Trạng thái</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($candidates as $key => $candidate): ?>
							<tr>
								<td><?php echo $candidate['cdd_id'] ?></td>
								<td><?php echo $candidate['cdd_fullname'] ?></td>
								<td><?php echo $candidate['prv_name'] ?></td>
								<td><?php echo $candidate['cdd_email'] ?></td>
								<td><?php echo $candidate['cdd_phone'] ?></td>
								<td><?php echo $candidate['cdd_degree'] ?></td>
								<td><?php echo $candidate['cdd_skill'] ?></td>
								<td><?php echo $candidate['cdd_expected_salary'] ?></td>
								<td><?php echo ($candidate['cdd_is_approved']==0) ? '<span class="label label-warning">Đang chờ</span>' : '<span class="label label-info">Đã duyệt</span>' ; ?></td>
								<td>
									<a href="<?php echo base_url('recruitment/is_approved/1/'.$candidate['cdd_id']) ?>" class="btn btn-xs btn-warning">Bỏ dấu</a>
									<a href="<?php echo base_url('recruitment/is_approved/2/'.$candidate['cdd_id']) ?>" class="btn btn-xs btn-success">Đánh dấu</a>
									<a onclick="return confirm('Bạn chắc chắn muốn xóa')" href="<?php echo base_url('recruitment/is_approved/0/'.$candidate['cdd_id']) ?>" class="btn btn-xs btn-danger">Xóa</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->