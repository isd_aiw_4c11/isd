<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-8 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Sửa tin tuyển dụng</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form action="<?php echo base_url('recruitment/update_news_exec') ?>" method="POST">
					<?php foreach ($newss as $key => $news): ?>
						<input type="hidden" name="post_id" value="<?php echo $news['pst_id'] ?>">
						<div class="form-group">
							<label for="">Tiêu đề</label>
							<input type="text" class="form-control" name="post[pst_title]" value="<?php echo $news['pst_title'] ?>">
						</div>
						<div class="form-group">
							<label for="">Nội dung</label>
							<textarea type="text" class="textarea form-control" rows="10" name="post[pst_content]"><?php echo $news['pst_content'] ?></textarea>
						</div>
					<?php endforeach ?>
					<button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Lưu lại</button>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->