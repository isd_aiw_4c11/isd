<!-- header logo: style can be found in header.less -->
<header class="header">
	<a href="<?php echo base_url(); ?>" class="logo" style="font-family: sans-serif; text-transform: uppercase">
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		BMK Company
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<?php if ($this->session->userdata('sta_id') != null): ?>
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-user"></i>
							<span><?php echo $this->session->userdata('sta_fullname') ?> <i class="caret"></i></span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header bg-light-blue">
								<img src="<?php echo base_url('public/avatar.png') ?>" class="img-circle" alt="User Image" />
								<p><?php echo $this->session->userdata('sta_fullname') ?></p>
							</li>
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="<?php echo base_url('staff/current_profile_detail') ?>" class="btn btn-default btn-flat">Đổi thông tin</a>
								</div>
								<div class="pull-right">
									<a href="<?php echo base_url('staff/logout') ?>" class="btn btn-default btn-flat">Đăng xuất</a>
								</div>
							</li>
						</ul>
					</li>
				<?php else: ?>
					<li><a href="<?php echo base_url('staff/login') ?>"><i class="fa fa-unlock"></i> Đăng nhập</a></li>
				<?php endif ?>
			</ul>
		</div>
	</nav>
</header>

<div class="wrapper row-offcanvas row-offcanvas-left">