<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="table" class="data-table">
						<thead>
							<tr>
								<th>Mã NV</th>
								<th>Họ tên</th>
								<th>Số tiền ứng</th>
								<th>Thời gian</th>
								<th>Trạng thái</th>
								<th>Thao tác</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($requests as $key => $request): ?>
								<?php
									$month = date("m",strtotime($request['adr_date']));
									$year = date("Y",strtotime($request['adr_date']));
								?>
								<tr>
									<td><?php echo $request['sta_username'] ?></td>
									<td><?php echo $request['sta_firstname'].' '.$request['sta_lastname'] ?></td>
									<td><?php echo $request['adr_amount'] ?></td>
									<td><?php echo 'Tháng '.$month.' Năm '.$year ?></td>
									<?php if ($request['adr_status'] == 0): ?>
										<td><span class="label label-warning">Đang chờ</span></td>
									<?php elseif ($request['adr_status'] == 1): ?>
										<td><span class="label label-danger">Đã hủy</span></td>
									<?php else: ?>
										<td><span class="label label-success">Đã ứng</span></td>
									<?php endif ?>
									<td>
										<a class="btn btn-sm btn-danger" href="<?php echo base_url('advance/handle_advance_request/1/'.$request['adr_id']) ?>">Hủy</a>
										<a class="btn btn-sm btn-success" href="<?php echo base_url('advance/handle_advance_request/2/'.$request['adr_id']) ?>">Duyệt</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
