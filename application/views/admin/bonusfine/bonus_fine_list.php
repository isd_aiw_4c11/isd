<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<div class="box-title"><a href="<?php echo base_url('bonusfine/bonus_fine_detail/') ?>" class="btn btn-success">Tạo thưởng/phạt</a></div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="table" class="data-table">
						<thead>
							<tr>
								<th>Mã NV</th>
								<th>Họ tên</th>
								<th>Thưởng / Phạt</th>
								<th>Số tiền</th>
								<th>Thời gian</th>
								<th>Thao tác</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($bfs as $key => $bf): ?>
								<?php
									$month = date("m",strtotime($bf['bnf_date']));
									$year = date("Y",strtotime($bf['bnf_date']));
								?>
								<tr>
									<td><?php echo $bf['sta_username'] ?></td>
									<td><?php echo $bf['sta_firstname'].' '.$bf['sta_lastname'] ?></td>
									<?php if ($bf['bnf_type'] == 0): ?>
										<td><span class="label label-danger">Phạt</span></td>
									<?php else: ?>
										<td><span class="label label-success">Thưởng</span></td>
									<?php endif ?>
									<td><?php echo number_format($bf['bnf_amount'], 0, ',', ' ') ?> đ</td>
									<td><?php echo 'Tháng '.$month.' Năm '.$year ?></td>
									<td>
										<a class="btn btn-sm btn-success" href="<?php echo base_url('bonusfine/bonus_fine_detail/'.$bf['bnf_id']) ?>">Sửa</a>
										<a class="btn btn-sm btn-danger" href="<?php echo base_url('bonusfine/remove_bonus_fine/'.$bf['bnf_id']) ?>">Xóa</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
