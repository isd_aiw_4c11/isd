<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<?php if (!isset($bfs)): ?>
		<div class="col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h2 class="box-title">Tạo thưởng phạt</h2>
				</div><!-- /.box-header -->
				<div class="box-body">
					<form class="row" action="<?php echo base_url('bonusfine/add_bonus_fine') ?>" method="POST">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="">Nhân viên</label>
								<select class="form-control" name="bnf[bnf_staff_id]" id="">
									<?php foreach ($staffs as $key => $staff): ?>
										<option value="<?php echo $staff['sta_id'] ?>"><?php echo $staff['sta_firstname'].' '.$staff['sta_lastname'] ?></option>
									<?php endforeach ?>
								</select>
							</div>

							<div class="form-group">
								<label for="">Loại</label>
								<span class="radio"><label for="">
									<input type="radio" name="bnf[bnf_type]" id="bonus" value="1" checked> Thưởng
								</label></span>
								<span class="radio"><label for="">
									<input type="radio" name="bnf[bnf_type]" id="fine" value="0"> Phạt
								</label></span>
							</div>

							<div class="form-group">
								<label for="">Số tiền</label>
								<input class="form-control" type="text" name="bnf[bnf_amount]">
							</div>

							<div class="form-group">
								<label for="">Ngày tháng</label>
								<input class="form-control" type="text" name="bnf[bnf_date]" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="date" autocomplete="off">
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label for="">Lý do</label>
								<textarea class="form-control" name="bnf[bnf_about]" id="" cols="30" rows="13"></textarea>
							</div>
						</div>

						<div class="col-xs-12">
							<button class="btn btn-success" type="submit">Tạo</button>
						</div>

					</form>
				</div><!-- /.box-body -->
				<div class="box-footer">
				</div><!-- /.box-footer -->
			</div>
		</div><!-- ./col -->
	<?php else: ?>
		<div class="col-sm-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h2 class="box-title">Sửa thưởng phạt</h2>
				</div><!-- /.box-header -->
				<div class="box-body">
					
					<form class="row" action="<?php echo base_url('bonusfine/update_bonus_fine') ?>" method="POST">
						<?php foreach ($bfs as $key => $bf): ?>
							<div class="col-sm-6">
								
								<input type="hidden" name="bnf[bnf_id]" value="<?php echo $bf['bnf_id'] ?>">

								<div class="form-group">
									<label for="">Nhân viên</label>
									<select class="form-control" name="bnf[bnf_staff_id]" id="">
										<?php foreach ($staffs as $key => $staff): ?>
											<option
												value="<?php echo $staff['sta_id'] ?>"
												<?php echo ($staff['sta_id'] == $bf['bnf_staff_id']) ? 'selected' : '' ; ?>
											>
												<?php echo $staff['sta_firstname'].' '.$staff['sta_lastname'] ?>
											</option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="form-group">
									<label for="">Loại</label>
									<span class="radio"><label for="">
										<input type="radio" name="bnf[bnf_type]" id="bonus" value="1" <?php echo ($bf['bnf_type'] == 1) ? 'checked' : '' ; ?>> Thưởng
									</label></span>
									<span class="radio"><label for="">
										<input type="radio" name="bnf[bnf_type]" id="fine" value="0" <?php echo ($bf['bnf_type'] == 0) ? 'checked' : '' ; ?>> Phạt
									</label></span>
								</div>

								<div class="form-group">
									<label for="">Số tiền</label>
									<input class="form-control" type="text" name="bnf[bnf_amount]" value="<?php echo $bf['bnf_amount'] ?>">
								</div>

								<div class="form-group">
									<label for="">Ngày tháng</label>
									<input class="form-control" type="text" name="bnf[bnf_date]" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="date" autocomplete="off" value="<?php echo $bf['bnf_date'] ?>">
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label for="">Lý do</label>
									<textarea class="form-control" name="bnf[bnf_about]" id="" cols="30" rows="13"><?php echo $bf['bnf_about'] ?></textarea>
								</div>
							</div>
						<?php endforeach ?>
						<div class="col-xs-12">
							<button class="btn btn-success" type="submit">Lưu thay đổi</button>
						</div>
					</form>

				</div><!-- /.box-body -->
				<div class="box-footer">
				</div><!-- /.box-footer -->
			</div>
		</div><!-- ./col -->
	<?php endif ?>

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
