<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>HRM</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- bootstrap 3.0.2 -->
	<?php echo link_tag('assets/admin/css/bootstrap.min.css'); ?>
	<!-- font Awesome -->
	<?php echo link_tag('assets/admin/css/font-awesome.min.css'); ?>
	<!-- bootstrap wysihtml5 - text editor -->
	<?php echo link_tag('assets/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>
	<!-- Ionicons -->
	<?php echo link_tag('assets/admin/css/ionicons.min.css'); ?>
	<!-- fullCalendar -->
	<?php echo link_tag('assets/admin/css/fullcalendar/fullcalendar.css'); ?>
	<!-- datatables -->
	<?php echo link_tag('assets/admin/css/datatables/dataTables.bootstrap.css'); ?>
	
	<?php echo link_tag('assets/admin/css/jQueryUI/jquery-ui-1.10.3.custom.min.css'); ?>
	<!-- Theme style -->
	<?php echo link_tag('assets/admin/css/AdminLTE.css'); ?>

	<!-- jQuery -->
	<script src="<?php echo base_url('assets/admin/js/jquery.js');?>"></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="<?php echo base_url('js/html5shiv.js');?>"></script>
	<script src="<?php echo base_url('js/respond.min.js');?>"></script>
	<![endif]-->

	<style>
		.content-header h1 small,
		.box-title {
			white-space: nowrap;
		}
	</style>

	</head>

<body class="skin-black">