<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<table class="table data-table" id="table ">
					<thead>
						<tr>
							<th>Mã tin</th>
							<th>Tiêu đề</th>
							<th>Ngày đăng</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($bulletin as $key => $pst): ?>
						<tr>
							<td><?php echo $pst['pst_id'] ?></td>
							<td><?php echo $pst['pst_title'] ?></td>
							<td><?php echo $pst['pst_time'] ?></td>
							<td>
								<a href="<?php echo base_url('bulletin/edit_bulletin').'/'.$pst['pst_id'] ?>" class="btn btn-info">Xem / Sửa</a>
								<a onclick="return confirm(' Bạn chắc chắn muốn xóa tin sự kiện này không?');" href="<?php echo base_url('bulletin/delete_bulletin/'.$pst['pst_id']) ?>" class="btn btn-danger">Xóa</a>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->

