<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<?php echo validation_errors(); ?>
				<?php echo form_open('bulletin/updateBulletin/'.$id); ?>
					<div class="form-group">
						<label for="">Tiêu đề</label>
						<input type="text" class="form-control" name="title"  value="<?php foreach ($bulletin as $key => $pst) {echo $pst['pst_title'];}?>">
					</div>
					<div class="form-group">
						
					</div>
					<div class="form-group">
						<label for="">Nội dung</label>
						<textarea type="text" class="form-control textarea" name="content" rows="10"><?php foreach ($bulletin as $key => $pst) {echo $pst['pst_content'];}?></textarea>
					</div>
			
					<button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu</button>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
