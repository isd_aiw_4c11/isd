<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-8 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Quản lý nghỉ phép</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<table class="table">
					<thead>
						<tr>
							<th>Ngày tháng</th>
							<th>Nhân viên</th>
							<th>Lý do</th>
							<th>Tình trạng</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($abrs as $key => $abr): ?>
							<tr>
								<td><?php echo $abr['abr_date'] ?></td>
								<td><?php echo $abr['sta_firstname'].' '.$abr['sta_lastname'] ?></td>
								<td><?php echo $abr['abr_reason'] ?></td>
								<td><?php echo ($abr['abr_is_approved']==0) ? '<span class="label label-warning">Đang chờ</span>' : '<span class="label label-info">Đã duyệt</span>' ; ?></td>
								<td>
									<a href="<?php echo base_url('absence/handle_absence_request/1/'.$abr['abr_id']) ?>" class="btn btn-xs btn-info">Duyệt</a>
									<a href="<?php echo base_url('absence/handle_absence_request/0/'.$abr['abr_id']) ?>" class="btn btn-xs btn-warning">Không duyệt</a>
									<a onclick="confirm('Bạn chắc chắn muốn xóa?');" href="<?php echo base_url('absence/remove_absence_request/'.$abr['abr_id']) ?>" class="btn btn-xs btn-danger">Xóa</a>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->