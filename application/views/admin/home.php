<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	
		<div class="row" style="margin-bottom: 30px;">
			<!-- Small boxes (Stat box) -->
			<div class="col-sm-3">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>
							Chấm công
						</h3>
						<br/>
						<br/>
					</div>
					<div class="icon">
						<i class="fa fa-calendar"></i>
					</div>
					<a href="<?php echo base_url('attendance') ?>" class="small-box-footer">
						Chi tiết <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div><!-- ./col -->
			<div class="col-sm-3">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>
							Lương
						</h3>
						<br/>
						<br/>
					</div>
					<div class="icon">
						<i class="fa fa-dollar"></i>
					</div>
					<a href="<?php echo base_url('salary') ?>" class="small-box-footer">
						Chi tiết <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div><!-- ./col -->
			<?php if (bmk_user_is_admin_2($this->session->userdata('sta_permission'))): ?>
				<div class="col-sm-3">
					<!-- small box -->
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3>
								Nhân viên
							</h3>
							<br/>
							<br/>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="<?php echo base_url('staff') ?>" class="small-box-footer">
							Chi tiết <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div><!-- ./col -->
			<?php endif ?>
			<div class="col-sm-3">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3>
							Bảng tin
						</h3>
						<br/>
						<br/>
					</div>
					<div class="icon">
						<i class="fa fa-bullhorn"></i>
					</div>
					<a href="<?php echo base_url('bulletin') ?>" class="small-box-footer">
						Chi tiết <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div><!-- ./col -->
		</div>

		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-md-6 connectedSortable"> 
				<!-- quick email widget -->
				<div class="box box-info">
					<div class="box-header">
						<i class="fa fa-envelope"></i>
						<h3 class="box-title">Tạo tin nhanh</h3>
						<!-- tools box -->
						<div class="pull-right box-tools">
							<button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
						</div><!-- /. tools -->
					</div>
					<?php echo validation_errors(); ?>
					<form action="<?php echo base_url('welcome/add_new_bulletin') ?>" method="POST">
						<div class="box-body">
							<div class="form-group">
								<label for="">Tiêu đề</label>
								<input type="text" class="form-control" name="title" >
							</div>
							<div class="form-group">
								<label for="">Nội dung</label>
								<textarea type="text" class="textarea form-control" name="content" rows="10"></textarea>
							</div>
						</div>
						<div class="box-footer clearfix">
							<button type="submit" name="submit" class="btn btn-primary pull-right">Tạo tin <i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</form>
				</div>
			</section><!-- /.Left col -->

			<div class="col-md-6">
				<ul class="timeline">
					<?php $latestDate = ''; ?>
					<?php foreach ($posts as $key => $post): ?>
						<?php if (strcmp( $latestDate, date("dmy", strtotime($post['pst_time']))) != 0 ): ?>
							<!-- timeline time label -->
							<li class="time-label">
								<span class="bg-red">
									<?php echo date("d-m-y", strtotime($post['pst_time'])); ?>
								</span>
							</li>
						<?php endif ?>
						<!-- /.timeline-label -->
						<!-- timeline item -->
						<li>
							<!-- timeline icon -->
							<i class="fa fa-bullhorn bg-aqua"></i>
							<div class="timeline-item">
									<span class="time"><i class="fa fa-clock-o"></i> <?php echo $myFormatForView = date("g:i A", strtotime($post['pst_time'])); ?></span>
									<h3 class="timeline-header"><a href="#"><?php echo $post['pst_title'] ?></a></h3>
									<div class="timeline-body">
										<?php echo $post['pst_content'] ?>
									</div>
							</div>
						</li>
						<!-- END timeline item -->
						<?php $latestDate = date("dmy", strtotime($post['pst_time'])); ?>
					<?php endforeach ?>
				</ul>
			</div>

		</div><!-- /.row (main row) -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->