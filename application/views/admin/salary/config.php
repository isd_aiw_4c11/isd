<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-6 col-xs-12">
		<div class="box box-primary">
			<div class="box-body">
				<form action="<?php echo base_url('salary/config_update') ?>" method="POST">
						<?php foreach ($configs as $key => $config): ?>
							<div class="form-group">
								<label for="">Số giờ / 1 công tiêu chuẩn</label>
								<input type="text" value="<?php echo $config['slc_standard_daywork_hours'] ?>" name="config[slc_standard_daywork_hours]" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Số công tiêu chuẩn</label>
								<input type="text" value="<?php echo $config['slc_standard_points'] ?>" name="config[slc_standard_points]" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Thưởng đủ số công tiêu chuẩn (đồng)</label>
								<input type="text" value="<?php echo $config['slc_attendance_bonus'] ?>" name="config[slc_attendance_bonus]" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Tỷ lệ lương ngoài giờ (% so với lương bình thường)</label>
								<input type="text" value="<?php echo $config['slc_extra_point_rate'] ?>" name="config[slc_extra_point_rate]" class="form-control">
							</div>
							<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>  Lưu</button>
						<?php endforeach ?>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
