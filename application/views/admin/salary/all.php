<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title" role="alert">
					Tháng <span class="label label-info" style="font-size: 16px"><?php echo $month ?></span> Năm <span class="label label-info" style="font-size: 16px"><?php echo $year ?></span>
				</h3>
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<table class="table table-hover data-table">
					<thead>
						<tr>
							<th>Mã NV</th>
							<th>Họ & Tên</th>
							<th>Số công tiêu chuẩn</th>
							<th>Số công ngoài giờ</th>
							<th>Đã ứng</th>
							<th>Thưởng</th>
							<th>Phạt</th>
							<th>Thành tiền</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($staffs as $key => $staff):
							$attHourSum = 0;
							$attExtraHourSum = 0;
							$attPointSum = 0;
							$attExtraPointSum = 0;
							foreach ($attendances as $key => $attendance) {
								if ($attendance['att_staff_id'] == $staff['sta_id']) {
									// Atendance hours
									$attHourSum += $attendance['att_point'];
									$attExtraHourSum += $attendance['att_extra_point'];
									// Convert to attendance points
									$attPointSum += $attendance['att_point'] / $configs[0]['slc_standard_daywork_hours'];
									$attExtraPointSum += $attendance['att_extra_point'] / $configs[0]['slc_standard_daywork_hours'];
								}
							}
							// Tính tiền đã ứng
							$advanceSum = 0;
							foreach ($advances as $key => $advance) {
								if (($advance['adr_staff_id'] == $staff['sta_id']) && ($advance['adr_status'] == 2)) {
									$advanceSum += $advance['adr_amount'];
								}
							}
							// Tính tiền thưởng phạt
							$bonusSum = 0;
							$fineSum = 0;
							foreach ($bfs as $key => $bf) {
								if ($bf['bnf_staff_id'] == $staff['sta_id']) {
									if ($bf['bnf_type'] == 1) {
										$bonusSum += $bf['bnf_amount'];
									} else {
										$fineSum += $bf['bnf_amount'];
									}
								}
							}
							// Kiểm tran xem đủ 26 công để thưởng hay không
							$bonusStandardPoints = ($attPointSum >= $configs[0]['slc_standard_points']) ? $configs[0]['slc_attendance_bonus'] : 0 ;
							// CALCULATE SUM
							$moneySum = 
								// Tiền theo công tiêu chuẩn
								$attPointSum * $staff['sta_single_attendance_amount']
								// Tiền theo công ngoài giờ
							+ 	$attExtraPointSum * ($staff['sta_single_attendance_amount'] + $staff['sta_single_attendance_amount'] * $configs[0]['slc_extra_point_rate'] / 100)
								// Thưởng làm đủ số công tiêu chuẩn
							+	$bonusStandardPoints
							- $advanceSum
							+ $bonusSum
							- $fineSum
							;

							// USE FOR PRINTITNG
							$moneyStandardSum = $attPointSum * $staff['sta_single_attendance_amount'];
							$moneyExtraSum = $attExtraPointSum * ($staff['sta_single_attendance_amount'] + $staff['sta_single_attendance_amount'] * $configs[0]['slc_extra_point_rate'] / 100);
						?>
						<?php if ($staff['sta_permission'] < 2): ?>
							<tr>
								<td><?php echo $staff['sta_username']; ?></td>
								<td><?php echo $staff['sta_firstname'].' '.$staff['sta_lastname'] ?></td>
								<td class="<?php echo ($attPointSum >= $configs[0]['slc_standard_points']) ? 'success' : '' ; ?>">
									<strong><?php echo $attHourSum/8 ?></strong> (<?php echo $attHourSum ?> giờ)
									<p style="margin: 0;">= <?php echo number_format($moneyStandardSum, 0, ',', ' ') ?> đ</p>
									<?php if ($attPointSum >= $configs[0]['slc_standard_points']): ?>
										<p style="margin: 0;">+ <?php echo number_format($bonusStandardPoints, 0, ',', ' ') ?> đ (>26 công)</p>
									<?php endif ?>
								</td>
								<td>
									<strong><?php echo $attExtraHourSum/8 ?></strong> (<?php echo $attExtraHourSum ?>giờ)
									<p style="margin: 0;">= <?php echo number_format($moneyExtraSum, 0, ',', ' ') ?> đ</p>
								</td>
								<td class="<?php echo ($advanceSum > 0) ? 'danger' : '' ; ?>"><?php echo number_format($advanceSum, 0, ',', ' ') ?> đ</td>
								<td class="<?php echo ($bonusSum > 0) ? 'success' : '' ; ?>"><?php echo number_format($bonusSum, 0, ',', ' ') ?> đ</td>
								<td class="<?php echo ($fineSum > 0) ? 'danger' : '' ; ?>"><?php echo number_format($fineSum, 0, ',', ' ') ?> đ</td>
								<td style="font-weight: 700"><?php echo number_format($moneySum, 0, ',', ' ') ?> đ</td>
							</tr>
						<?php endif ?>
						<?php endforeach ?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<form class="form-inline" method="post" action="<?php echo base_url('salary/calculate_salary') ?>">
					<div class="form-group">
						<select name="month" id="" class="form-control" required>
							<?php
								for ($i = 1; $i <= 12; $i++) {
									if ($month == $i) {
										echo '<option value="'.$i.'" selected>'.$i.'</option>';
									} else {
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<select name="year" id="" class="form-control" required>
							<?php
								for ($i = (int)date('Y'); $i >= (int)date('Y') - 3; $i--) {
									if ($year == $i) {
										echo '<option value="'.$i.'" selected>'.$i.'</option>';
									} else {
										echo '<option value="'.$i.'">'.$i.'</option>';
									}
								}
							?>
						</select>
					</div>
					<button type="submit" class="btn btn-success">Đi &rarr;</button>
					<a href="<?php echo base_url('salary/calculate_salary/'.$month.'/'.$year); ?>" class="btn btn-info btn-print pull-right"><i class="fa fa-print"></i> In trang này</a>
				</form>
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	<div class="col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h2 class="box-title">Thông số tính lương</h2>
			</div><!-- /.box-header -->
			<div class="box-body row">
				<?php foreach ($configs as $key => $config): ?>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Số giờ / 1 công tiêu chuẩn</label>
							<input type="text" value="<?php echo $config['slc_standard_daywork_hours'] ?> giờ" class="form-control" disabled>
						</div>
						<div class="form-group">
							<label for="">Số công tiêu chuẩn</label>
							<input type="text" value="<?php echo $config['slc_standard_points'] ?>" class="form-control" disabled>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="">Thưởng đủ số công tiêu chuẩn (đồng)</label>
							<input type="text" value="<?php echo number_format($config['slc_attendance_bonus'], 0, ',', ' '); ?> đ" class="form-control" disabled>
						</div>
						<div class="form-group">
							<label for="">Tỷ lệ lương ngoài giờ (% so với lương bình thường)</label>
							<input type="text" value="<?php echo $config['slc_extra_point_rate'] ?> %" class="form-control" disabled>
						</div>
					</div>
					<div class="col-xs-12">
						<a href="<?php echo base_url('salary/config') ?>">Sửa tham số bảng lương &rarr;</a>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>


	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->