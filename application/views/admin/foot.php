	</div><!-- ./wrapper -->

	<!-- jQuery UI 1.10.3 -->
	<script src="<?php echo base_url('assets/admin/js/jquery-ui-1.10.3.min.js');?>"></script>
	<!-- Print page -->
	<script src="<?php echo base_url('assets/admin/js/plugins/print-page.js');?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url('assets/admin/js/bootstrap.min.js');?>"></script>
	<!-- Raphael -->
	<script src="<?php echo base_url('assets/admin/js/raphael-min.js');?>"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="<?php echo base_url('assets/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
	<!-- Datatable -->
	<script src="<?php echo base_url('assets/admin/js/plugins/datatables/jquery.dataTables.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/plugins/datatables/dataTables.bootstrap.js');?>"></script>
	<!-- fullCalendar -->
	<script src="<?php echo base_url('assets/admin/js/plugins/fullcalendar/fullcalendar.min.js');?>"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url('assets/admin/js/plugins/iCheck/icheck.min.js');?>"></script>
	<!-- InputMask -->
	<script src="<?php echo base_url('assets/admin/js/plugins/input-mask/jquery.inputmask.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/plugins/input-mask/jquery.inputmask.date.extensions.js');?>"></script>
	<script src="<?php echo base_url('assets/admin/js/plugins/input-mask/jquery.inputmask.extensions.js');?>"></script>
	<!-- Morris.js charts -->
	<script src="<?php echo base_url('assets/admin/js/plugins/morris/morris.min.js');?>"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/admin/js/AdminLTE/app.js');?>"></script>
	
	<!-- Custom JS) -->
	<script src="<?php echo base_url('assets/admin/js/custom.js');?>"></script>

	</body>
</html>