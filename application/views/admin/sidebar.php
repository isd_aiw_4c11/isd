<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder=""/>
				<span class="input-group-btn">
					<button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php
			$is_welcome = '';
			$is_staff = '';
			$is_attendance = '';
			$is_salary = '';
			$is_recruitment = '';
			$is_absence = '';
			$is_bulletin = '';
			$is_event = '';
			switch ($pageGroupId) {
				case 'welcome':
					$is_welcome = 'active';
					break;
				case 'staff':
					$is_staff = 'active';
					break;
				case 'attendance':
					$is_attendance = 'active';
					break;
				case 'salary':
					$is_salary = 'active';
					break;
				case 'recruitment':
					$is_recruitment = 'active';
					break;
				case 'absence':
					$is_absence = 'active';
					break;
				case 'bulletin':
					$is_bulletin = 'active';
					break;
				case 'event':
					$is_event = 'active';
					break;
				default:
					# code...
					break;
			}
		?>
		<ul class="sidebar-menu">
			<li class="<?php echo $is_welcome ?>">
				<a href="<?php echo base_url('welcome') ?>">
					<i class="fa fa-dashboard"></i> <span>Tổng quan</span>
				</a>
			</li>
			<?php if (bmk_user_is_admin_2($this->session->userdata('sta_permission'))): ?>
				<li class="treeview <?php echo $is_staff ?>">
					<a href="#">
						<i class="fa fa-users"></i>
						<span>Nhân viên</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('staff/view_staffs') ?>"><i class="fa fa-angle-double-right"></i> Danh sách</a></li>
						<li><a href="<?php echo base_url('staff/add_staff') ?>"><i class="fa fa-angle-double-right"></i> Thêm nhân viên</a></li>
						<li><a href="<?php echo base_url('department/manage_department') ?>"><i class="fa fa-angle-double-right"></i> Quản lý phòng ban</a></li>
					</ul>
				</li>
			<?php endif ?>
			<?php if (bmk_user_is_admin_1($this->session->userdata('sta_permission'))): ?>
				<li class="treeview <?php echo $is_attendance ?>">
					<a href="#">
						<i class="fa fa-calendar"></i>
						<span>Chấm công</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('attendance/view_attendance') ?>"><i class="fa fa-angle-double-right"></i> Bảng công</a></li>
						<li><a href="<?php echo base_url('attendance/add_attendance_table') ?>"><i class="fa fa-angle-double-right"></i> Tạo bảng chấm công</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $is_salary ?>">
					<a href="#">
						<i class="fa fa-dollar"></i>
						<span>Quản lý lương</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('salary/calculate_salary') ?>"><i class="fa fa-angle-double-right"></i> Bảng lương</a></li>
						<li><a href="<?php echo base_url('salary/config') ?>"><i class="fa fa-angle-double-right"></i> Tham số bảng lương</a></li>
						<li><a href="<?php echo base_url('advance/manage_advance_request') ?>"><i class="fa fa-angle-double-right"></i> Quản lý ứng tiền</a></li>
						<li><a href="<?php echo base_url('bonusfine/manage_bonus_fine') ?>"><i class="fa fa-angle-double-right"></i> Quản lý thưởng phạt</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $is_recruitment ?>">
					<a href="#">
						<i class="fa fa-user"></i>
						<span>Quản lý tuyển dụng</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('recruitment/news') ?>"><i class="fa fa-angle-double-right"></i> Tin tuyển dụng</a></li>
						<li><a href="<?php echo base_url('recruitment/candidates') ?>"><i class="fa fa-angle-double-right"></i> Đơn ứng tuyển</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $is_absence ?>">
					<a href="#">
						<i class="fa fa-paper-plane"></i>
						<span>Nghỉ phép</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('absence/view_absence_requests') ?>"><i class="fa fa-angle-double-right"></i> Quản lý nghỉ phép</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $is_bulletin ?>">
					<a href="#">
						<i class="fa fa-bullhorn"></i>
						<span>Quản lý bảng tin</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('bulletin/view_all_bulletins') ?>"><i class="fa fa-angle-double-right"></i> Danh sách</a></li>
						<li><a href="<?php echo base_url('bulletin/add_new_bulletin') ?>"><i class="fa fa-angle-double-right"></i> Tạo tin mới</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $is_event ?>">
					<a href="#">
						<i class="fa fa-calendar"></i>
						<span>Quản lý sự kiện</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('event/view_all_events') ?>"><i class="fa fa-angle-double-right"></i> Danh sách</a></li>
						<li><a href="<?php echo base_url('event/add_new_event') ?>"><i class="fa fa-angle-double-right"></i> Tạo tin mới</a></li>
					</ul>
				</li>
			<?php endif ?>

		</ul>
	</section>
	<!-- /.sidebar -->
</aside>