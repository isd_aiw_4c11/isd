
?><!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<!-- <table class="table data-table" id="table"> -->
					<table class="table data-table">
					<thead>
						<tr>
							<th>Thời gian</th>
							<th>Địa điểm</th>
							<th>Nội dung</th>
							<th style="width: 150px;">Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php 	foreach ($events as $key => $event): ?>
						<tr>
						
							<td><?php echo $event['eve_date']; ?> </td>
							<td><?php echo $event['eve_place'] ?></td>
							<td><?php echo $event['eve_content'] ?></td>
							<td>
								<a href="<?php echo base_url('event/edit_event').'/'.$event['eve_id'] ?>" class="btn btn-info">Sửa</a>
								<a onclick="return confirm(' Bạn chắc chắn muốn xóa tin sự kiện này không?');" href="<?php echo base_url('event/delete_event/'.$event['eve_id']) ?>" class="btn btn-danger">Xóa</a>
							</td>
						</tr>
						
						<?php 
							endforeach;
						?>
					</tbody>
				</table>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
