<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-8 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body table-responsive">
				<?php echo validation_errors(); ?>
				<?php echo form_open('event/add_new_event'); ?>
					
					<div class="form-group">
						<label for="">Thời gian</label>
						<input type="text"  name="date" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="date"/>
					</div>
					<div class="form-group">
						<label for="">Địa điểm</label>
						<input type="text" class="form-control" name="place" >
					</div>
					<div class="form-group">
						<label for="">Nội dung</label>
						<textarea type="text" class="form-control" name="content" rows="10"></textarea>
					</div>
			
				<button type="submit" name="submit" class="btn btn-primary">Tạo</button>
			</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
