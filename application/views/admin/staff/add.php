<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body">
				<form action="<?php echo base_url('staff/add_staff_exec') ?>" method="POST">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<h3>Sơ yếu lý lịch</h3>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Họ (và tên đệm nếu có)</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-user"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_firstname'] ?>" name="sta[sta_firstname]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Tên</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-user"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_lastname'] ?>" name="sta[sta_lastname]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Số CMND</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-barcode"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_civilianId'] ?>" name="sta[sta_civilianId]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Ngày sinh</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_dob'] ?>" name="sta[sta_dob]" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="date" autocomplete="off"/>
								</div>
								<small><em>Nhập theo dạng năm-tháng-ngày</em></small>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Quê quán</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-home"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_hometown'] ?>" name="sta[sta_hometown]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Thường trú</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-paper-plane"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_address'] ?>" name="sta[sta_address]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Email</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_email'] ?>" name="sta[sta_email]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Điện thoại</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-phone"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_phone'] ?>" name="sta[sta_phone]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
						</div>


						<div class="col-xs-12 col-sm-4">
							<h3>Công ty</h3>
							<div class="form-group">
								<label for="">Phòng ban</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-folder"></i>
									</div>
									<select class="form-control" name="sta_department_id" id="select-department" data-url="<?php echo base_url('ajax/get_position_by_department_id') ?>">
										<?php foreach ($departments as $key => $department): ?>
											<option value="<?php echo $department['dep_id'] ?>"><?php echo $department['dep_name'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="">Vị trí</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-align-left"></i>
									</div>
									<select class="form-control" name="sta[sta_position_id]" id="select-position" disabled="true">
										
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="">Ngày công (đồng)</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-dollar"></i>
									</div>
									<input type="text" class="form-control" name="sta[sta_single_attendance_amount]">
								</div>
							</div>
							<?php
								function contractStatus($sta_contract_deadline) {
									if (date('Y\-m\-d') >= $sta_contract_deadline)
										return false;
									return true;
								}
							?>
							<div class="form-group">
								<label for="">Thời hạn hợp đồng</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" value="<?php echo $temp['sta_contract_deadline'] ?>" name="sta[sta_contract_deadline]" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="date" autocomplete="off"/>
								</div>
								<small><em>Nhập theo dạng năm-tháng-ngày</em></small>
							</div>
							<!-- /.form-group -->
						</div>

						<div class="col-xs-12 col-sm-4">
							<h3>Tài khoản</h3>
							<div class="form-group">
								<label for="">Quyền hạn</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-unlock"></i>
									</div>
									<select class="form-control" name="sta[sta_permission]">
										<option value="0">Nhân viên</option>
										<option value="1">Admin cấp 1</option>
										<option value="2">Admin cấp 2</option>
									</select>
								</div>
							</div>
						</div>


					</div>

					<input type="submit" value="Lưu thay đổi" class="btn btn-success pull-right">
					<br/>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->

<script>
	$(function() {
		
	});
</script>