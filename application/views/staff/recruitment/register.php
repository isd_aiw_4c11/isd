<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">

	<?php if (isset($flash_message)): ?>
		<div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>!</b> <?php echo $flash_message; ?>
		</div>
	<?php endif ?>

	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				
			</div><!-- /.box-header -->
			<div class="box-body">
				<form method="POST" action="<?php echo base_url('recruitment/recruitment_submit') ?>" class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label>Họ tên</label>
							<input type="text" class="form-control" type="text" name="cdd[cdd_fullname]" required></input>
						</div>
						<div class="form-group">
							<label>Quê quán</label>
							<select class="form-control" name="cdd[cdd_province_id]" required>
								<?php foreach ($provinces as $key => $province): ?>
									<option value="<?php echo $province['prv_id'] ?>"><?php echo $province['prv_name'] ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="form-group">
							<label>Điện thoại</label>
							<input type="number" class="form-control" name="cdd[cdd_phone]" required></input>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control" name="cdd[cdd_email]" required></input>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							<label>Bằng cấp</label>
							<input type="text" class="form-control" name="cdd[cdd_degree]" required></input>
						</div>
						<div class="form-group">
							<label>Sở trường / Kỹ năng</label>
							<input type="text" class="form-control" name="cdd[cdd_skill]" required></input>
						</div>
						<div class="form-group">
							<label>Mức lương mong muốn</label>
							<input type="number" class="form-control" name="cdd[cdd_expected_salary]" required></input>
						</div>
					</div>

					<div class="col-xs-12">
						<button type="submit" class="btn btn-info">Gửi</button>
					</div>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
