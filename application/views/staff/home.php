<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-xs-12">
		<?php if (count($recruitments)>0): ?>
			<?php for ($i=0; $i<1; $i++): ?>
				<div class="jumbotron">
					<h2 style="margin-bottom: 30px"><?php echo $recruitments[$i]['pst_title'] ?></h2>
					<?php echo $recruitments[$i]['pst_content'] ?>
					<p><a style="margin-top: 15px;" class="btn btn-primary btn-lg" href="<?php echo base_url('recruitment/recruitment_form') ?>" role="button">Ứng tuyển ngay</a></p>
				</div>
			<?php endfor ?>
		<?php endif ?>
	</div>

	<div class="col-xs-12 col-sm-12">

		<ul class="timeline">
			<?php $latestDate = ''; ?>
			<?php foreach ($posts as $key => $post): ?>
				<?php if (strcmp( $latestDate, date("dmy", strtotime($post['pst_time']))) != 0 ): ?>
					<!-- timeline time label -->
					<li class="time-label">
						<span class="bg-red">
							<?php echo date("d-m-y", strtotime($post['pst_time'])); ?>
						</span>
					</li>
				<?php endif ?>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<!-- timeline icon -->
					<i class="fa fa-bullhorn bg-aqua"></i>
					<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i> <?php echo $myFormatForView = date("g:i A", strtotime($post['pst_time'])); ?></span>
							<h3 class="timeline-header"><a href="#"><?php echo $post['pst_title'] ?></a></h3>
							<div class="timeline-body">
								<?php echo $post['pst_content'] ?>
							</div>
					</div>
				</li>
				<!-- END timeline item -->
				<?php $latestDate = date("dmy", strtotime($post['pst_time'])); ?>
			<?php endforeach ?>
		</ul>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
