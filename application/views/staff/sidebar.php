<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search..."/>
				<span class="input-group-btn">
					<button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php
			$is_welcome = '';
			$is_staff = '';
			$is_attendance = '';
			$is_salary = '';
			$is_recruitment = '';
			$is_absence = '';
			$is_bulletin = '';
			$is_event = '';
			switch ($pageGroupId) {
				case 'welcome':
					$is_welcome = 'active';
					break;
				case 'staff':
					$is_staff = 'active';
					break;
				case 'attendance':
					$is_attendance = 'active';
					break;
				case 'salary':
					$is_salary = 'active';
					break;
				case 'recruitment':
					$is_recruitment = 'active';
					break;
				case 'absence':
					$is_absence = 'active';
					break;
				case 'bulletin':
					$is_bulletin = 'active';
					break;
				case 'event':
					$is_event = 'active';
					break;
				default:
					# code...
					break;
			}
		?>
		<ul class="sidebar-menu">
			<li class="<?php echo $is_welcome ?>">
				<a href="<?php echo base_url('welcome') ?>">
					<i class="fa fa-dashboard"></i> <span>Trang chủ</span>
				</a>
			</li>
			<li class="treeview <?php echo $is_recruitment ?>">
				<a href="#">
					<i class="fa fa-user"></i>
					<span>Tuyển dụng</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url('recruitment/recruitment_form') ?>"><i class="fa fa-angle-double-right"></i> Đăng ký dự tuyển</a></li>
				</ul>
			</li>
			<?php if ($this->session->userdata('sta_permission') || $this->session->userdata('sta_permission') != null): ?>
				<li class="treeview <?php echo $is_salary ?>">
					<a href="#">
						<i class="fa fa-paper-plane"></i>
						<span>Tiền lương</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('advance/request_advance') ?>"><i class="fa fa-angle-double-right"></i> Xin ứng tiền</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $is_absence ?>">
					<a href="#">
						<i class="fa fa-paper-plane"></i>
						<span>Nghỉ phép</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url('absence/request_absence') ?>"><i class="fa fa-angle-double-right"></i> Xin nghỉ</a></li>
					</ul>
				</li>
			<?php endif ?>

		</ul>
	</section>
	<!-- /.sidebar -->
</aside>