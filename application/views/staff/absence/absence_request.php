<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-6 col-xs-12">
		<div class="box box-primary">
			<div class="box-header">
				<h2 class="box-title">Nhập thông tin</h2>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form action="<?php echo base_url('absence/request_absence_exec') ?>" method="post">
					<input type="hidden" value="<?php echo $this->session->userdata('sta_id') ?>" name="abr[abr_staff_id]">
					<div class="form-group">
						<label for="">Ngày tháng</label>
						<input type="text" name="abr[abr_date]" class="form-control" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="date" autocomplete="off"/>
						<p style="font-size:12px; font-style: italic">Nhập theo dạng năm-tháng-ngày. Ví dụ 2015-01-05</p>
					</div>
					<div class="form-group">
						<label for="">Lý do</label>
						<textarea class="form-control" name="abr[abr_reason]" id="" cols="30" rows="10"></textarea>
					</div>
					<button type="submit" class="btn btn-success">Gửi</button>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
