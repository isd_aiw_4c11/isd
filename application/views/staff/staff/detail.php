<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $pageTitle ?>
			<small><?php echo $pageGroupTitle ?></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
	
	<div class="col-sm-12 col-xs-12">
		<?php if (isset($flash_message)): ?>
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<b>!</b> <?php echo $flash_message; ?>
			</div>
		<?php endif ?>
		<div class="box box-primary">
			<div class="box-header">
			</div><!-- /.box-header -->
			<div class="box-body">
				<form action="<?php echo base_url('staff/update_current_profile') ?>" method="POST">
					<a href="<?php echo base_url('staff/staff_detail/'.$staff[0]['sta_id']); ?>" class="btn btn-info btn-print"><i class="fa fa-print"></i> In trang này</a>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Lưu thay đổi</button>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<h3>Cập nhật thông tin</h3>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Họ (và tên đệm nếu có)</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-user"></i>
									</div>
									<input type="text" value="<?php echo $staff[0]['sta_firstname'] ?>" name="sta[sta_firstname]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Tên</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-user"></i>
									</div>
									<input type="text" value="<?php echo $staff[0]['sta_lastname'] ?>" name="sta[sta_lastname]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							
							<div class="form-group">
								<label for="">Thường trú</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-paper-plane"></i>
									</div>
									<input type="text" value="<?php echo $staff[0]['sta_address'] ?>" name="sta[sta_address]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Email</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</div>
									<input type="text" value="<?php echo $staff[0]['sta_email'] ?>" name="sta[sta_email]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
							<div class="form-group">
								<label for="">Điện thoại</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-phone"></i>
									</div>
									<input type="text" value="<?php echo $staff[0]['sta_phone'] ?>" name="sta[sta_phone]" class="form-control">
								</div>
							</div>
							<!-- /.form-group -->
						</div>

						<div class="col-xs-12 col-sm-4">
							<h3>Đổi mật khẩu</h3>
							<div class="form-group">
								<label for="">Nhập mật khẩu mới</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-key"></i>
									</div>
									<input type="password" class="form-control" name="sta[sta_password]" placeholder="">
								</div>
								<p style="font-size: 11px; font-style: italic">Để trống nếu không muốn đổi</p>
							</div>
							
						</div>

					</div>

					<br/>
				</form>
			</div><!-- /.box-body -->

			<div class="box-footer">
			</div><!-- /.box-footer -->
		</div>
	</div><!-- ./col -->

	</div><!-- /.row -->
	</section><!-- /.content -->
</aside><!-- /.right-side -->

<script>
	$(function() {
		
	});
</script>