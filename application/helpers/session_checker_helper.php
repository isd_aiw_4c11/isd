<?php

/**
 Session checker
 */
if(!function_exists('bmk_session_check')) {
	function bmk_session_check($type,$permission) {
		switch ($type) {
			case 'staff':
				if ($permission == null) {
					redirect(base_url('welcome'));
				}
				break;
			case 'admin_1':
				if ($permission < 1) {
					redirect(base_url('welcome'));
				}
				break;
			case 'admin_2':
				if ($permission < 2) {
					redirect(base_url('welcome'));
				}
				break;
		}
	}
}
/**
 Permission level
 */
if(!function_exists('bmk_user_is_admin_1')) {
	function bmk_user_is_admin_1($permission) {
		if ($permission >= 1)
			return true;
		return false;
	}
}
if(!function_exists('bmk_user_is_admin_2')) {
	function bmk_user_is_admin_2($permission) {
		if ($permission >= 2)
			return true;
		return false;
	}
}

?>