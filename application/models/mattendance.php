<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mattendance extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function getAttendancesByMonth($month, $year) {
		return $this->db
			-> query('SELECT * FROM attendance INNER JOIN staff WHERE att_staff_id = sta_id AND MONTH(att_date) = '.$month.' AND YEAR(att_date) = '.$year.' ORDER BY att_date ASC')
			-> result_array();
	}
	public function getAttendancesByDay($day, $month, $year) {
		return $this->db
			-> query('SELECT * FROM attendance INNER JOIN staff WHERE att_staff_id = sta_id AND DAY(att_date) = '.$day.' AND MONTH(att_date) = '.$month.' AND YEAR(att_date) = '.$year.' ORDER BY att_date ASC')
			-> result_array();
	}
	public function createAttendanceTable($staffs, $month, $year) {
		$daysInMonth = array (
			'1'   => '31',
			'2'   => '28',
			'3'   => '31',
			'4'   => '30',
			'5'   => '31',
			'6'   => '30',
			'7'   => '31',
			'8'   => '31',
			'9'   => '30',
			'10' => '31',
			'11' => '30',
			'12' => '31',
		);
		// Create number of record base on number of staffs
		$queryContent = '';
		$query ='';
		$date = '';
		foreach ($staffs as $key => $staffsIds) {
			// Create number of record for each staff base on number of day in month
			for ( $day=1; $day <= $daysInMonth[$month]; $day++ ) {
				$date = '"'.$year.'-'.$month.'-'.str_pad($day, 2, '0', STR_PAD_LEFT).'"';
				$queryContent .= '('.$staffsIds.', '.$date.'),';
			}
		}
		$query = rtrim('INSERT INTO attendance(att_staff_id, att_date) VALUE'.$queryContent, ",");
		$this->db->query($query);
	}
	public function checkAttendance($date, $staffsPoints) {
		foreach ($staffsPoints as $key => $staffPoints) {
			$condition = array(
				'att_staff_id' => $key,
				'att_date' => $date
			);
			$this->db
				->where($condition)
				->update('attendance', $staffPoints);
		}
	}
	/**
	 Absence
	 */
	public function insertAbsenceRequest($data) {
		$this->db->insert('absence_request', $data);
	}
	public function getAllAbsenceRequests() {
		return $this->db
			->join('staff', 'staff.sta_id = absence_request.abr_staff_id')
			->get('absence_request')
			->result_array();
	}
	public function handleAbsenceRequest($id, $data) {
		$this->db
			->where('abr_id',$id)
			->update('absence_request', $data);
	}
	public function deleteAbsenceRequest($id) {
		$this->db
			->where('abr_id',$id)
			->delete('absence_request');
	}
}