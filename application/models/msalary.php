<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msalary extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	/**
		Salary config
	*/
	public function getSalaryConfig() {
		return $this->db
			->select()
			->get('salary_config')
			->result_array();
	}
	public function updateSalaryConfig($data) {
		$this->db->update('salary_config',$data);
	}
	/**
		Calculate salary
	*/
	public function getAttendancesByMonth($month, $year) {
		return $this->db
			-> query('SELECT * FROM attendance WHERE MONTH(att_date) = '.$month.' AND YEAR(att_date) = '.$year.' ORDER BY att_date ASC')
			-> result_array();
	}
	/**
		Advance request
	*/
	public function insertAdvanceRequest($data) {
		$this->db->insert('advance_request', $data);
	}
	public function getAdvanceRequestByMonth($month, $year) {
		return $this->db
			-> query('SELECT * FROM advance_request INNER JOIN staff WHERE adr_staff_id = sta_id AND MONTH(adr_date) = '.$month.' AND YEAR(adr_date) = '.$year.' ORDER BY adr_date ASC')
			-> result_array();
	}
	public function getAdvancesRequest() {
		return $this->db
			->order_by('adr_date', 'DESC')
			->join('staff', 'staff.sta_id = advance_request.adr_staff_id')
			->get('advance_request')
			->result_array();
	}
	public function handleAdvanceRequest($id, $status) {
		$this->db
			->where('adr_id', $id)
			->update('advance_request', $status);
	}
	/**
		Bonus & fine
	*/
	public function getBonusFineByMonth($month, $year) {
		return $this->db
			-> query('SELECT * FROM bonus_fine INNER JOIN staff WHERE bnf_staff_id = sta_id AND MONTH(bnf_date) = '.$month.' AND YEAR(bnf_date) = '.$year.' ORDER BY bnf_date ASC')
			-> result_array();
	}
	public function getBonusFine($id = NULL) {
		if ($id == NULL) {
			return $this->db
				->order_by('bnf_date', 'DESC')
				->join('staff', 'staff.sta_id = bonus_fine.bnf_staff_id')
				->get('bonus_fine')
				->result_array();
		} else {
			return $this->db
				->where('bnf_id', $id)
				->join('staff', 'staff.sta_id = bonus_fine.bnf_staff_id')
				->get('bonus_fine')
				->result_array();
		}
	}
	public function addBonusFine($data) {
		$this->db->insert('bonus_fine', $data);
	}
	public function updateBonusFine($data) {
		$this->db
			->where('bnf_id', $data['bnf_id'])
			->update('bonus_fine', $data);
	}
	public function deleteBonusFine($id) {
		$this->db
			->where('bnf_id', $id)
			->delete('bonus_fine');
	}
}