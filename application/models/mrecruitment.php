<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mrecruitment extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	/**
		Candidate
	*/
	public function getAllCandidate() {
		return $this->db
			->select()
			->join('province', 'province.prv_id = candidate.cdd_province_id')
			->order_by('cdd_id', 'DESC')
			->get('candidate')
			->result_array();
	}
	public function deleteCandidate($id) {
		$this->db
			->where('cdd_id', $id)
			->delete('candidate');
	}
	public function markCandidate($id,$data) {
		$this->db
			->where('cdd_id', $id)
			->update('candidate', $data);
	}
	/**
		Recruitment
	*/
	public function createNews($data) {
		$this->db->insert('bulletin', $data);
	}
	public function getRecruitmentNews() {
		return $this->db
			->where('pst_is_recruitment', '1')
			->order_by('pst_time', 'DESC')
			->get('bulletin')
			->result_array();
	}
	public function getNews($id) {
		return $this->db
			->where('pst_id', $id)
			->order_by('pst_time', 'DESC')
			->get('bulletin')
			->result_array();
	}
	public function updateNews($id, $data) {
		return $this->db
			->where('pst_id', $id)
			->where('pst_is_recruitment', '1')
			->update('bulletin', $data);
	}
	public function deleteNews($id) {
		$this->db
			->where('pst_id', $id)
			->where('pst_is_recruitment', '1')
			->delete('bulletin');
	}
	public function getAllProvinces() {
		return $this->db
			->get('province')
			->result_array();
	}
	public function submitRecruitment($data) {
		$this->db->insert('candidate', $data);
	}
}