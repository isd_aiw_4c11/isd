<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mposition extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	/**
		For staff controller
	*/
	public function getAllPositions() {
		return $this->db
			->select()
			->get('position')
			-> result_array();
	}

	public function getPositionByDepartmentId($id) {
		return $this->db
			->select()
			->where('psn_department_id', $id)
			->get('position')
			-> result_array();
	}
}