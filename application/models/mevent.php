<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mevent extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function getEvents() {
		$this->db->select();
		$this->db->order_by("eve_date desc");
		$query= $this->db->get('event');
		return $query->result_array();
	}
	
	public function addNew($data) {
		return $this->db->insert('event',$data);
	}
	public function getEventById($id) {
		$this->db->query("select * from event");
		$this->db->where("eve_id","$id");
		$query= $this->db->get('event');
		return $query->result_array();
	}
	public function updateEvent($id, $data) {
		$this->db->where('eve_id',$id);
		return $this->db->update('event',$data);
	}
	public function delete($id) {
		$this->db
			->where('eve_id', $id)
			->delete('event');
	}
}