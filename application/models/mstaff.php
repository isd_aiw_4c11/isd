<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mstaff extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	/**
		For staff controller
	*/
	public function getStaffs() {
		return $this->db
			->select()
			->get('staff')
			-> result_array();
	}
	public function addStaff($data) {
		$this->db->insert('staff', $data);
	}
	public function getStaffsById($id) {
		return $this->db
			->where('sta_id', $id)
			->join('position', 'position.psn_id = staff.sta_position_id')
			->join('department', 'department.dep_id = position.psn_department_id')
			->get('staff')
			-> result_array();
	}
	public function updateProfile($data) {
		$this->db
			->where('sta_id', $data['sta_id'])
			->update('staff', $data);
	}
	public function login($data) {
		return $this->db
			->select()
			->where($data)
			->get('staff')
			-> result_array();
	}
	public function fireStaff($id) {
		$this->db
			->where('abr_staff_id', $id)
			->delete('absence_request');
		$this->db
			->where('adr_staff_id', $id)
			->delete('advance_request');
		$this->db
			->where('att_staff_id', $id)
			->delete('attendance');
		$this->db
			->where('bnf_staff_id', $id)
			->delete('bonus_fine');
		$this->db
			->where('sta_id', $id)
			->delete('staff');
	}
	/**
		For attendance controller
	*/
	public function getStaffWithAttendance($staffId, $month, $year) {
		return $this->db
			-> query('SELECT * FROM attendance WHERE att_staff_id = '.$staffId.' AND MONTH(att_date) = '.$month.' AND YEAR(att_date) = '.$year)
			-> result_array();
	}
	public function getLatestStaff() {
		return $this->db
			->select_max('sta_id')
			->get('staff')
			->result_array();
	}
}