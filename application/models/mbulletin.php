<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mbulletin extends CI_Model{
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	public function getBulletin() {
		return $this->db
			->where('pst_is_recruitment','0')
			->order_by('pst_time', 'DESC')
			->get('bulletin')
			->result_array();
	}
	public function addNew($data) {
		return $this->db->insert('bulletin',$data);
	}
	public function delete($id) {
		$this->db
			->where('pst_id', $id)
			->delete('bulletin');
	}
	public function getPostById($id) {
		$this->db->select("*");
		$this->db->from('bulletin');
		$this->db->where("pst_id","$id");
		$query= $this->db->get();
		return $query->result_array();
	}
	public function updatePost($id, $data) {
		$this->db->where('pst_id',$id);
		return $this->db->update('bulletin',$data);
	}
}