<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdepartment extends CI_Model{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function getAllDepartments() {
		return $this->db
			->select()
			->get('department')
			-> result_array();
	}
	public function getAllPositions() {
		return $this->db
			->select()
			->get('position')
			-> result_array();
	}
	public function getAllPositionsInDepartment($id) {
		return $this->db
			->select()
			->where('psn_department_id', $id)
			->get('position')
			-> result_array();
	}
	public function createDepartment($data) {
		$this->db->insert('department', $data);
	}
	public function createPosition($data) {
		$this->db->insert('position', $data);
	}
	public function deleteDepartment($id) {
		$this->db
			->where('psn_department_id', $id)
			->delete('position');
		$this->db
			->where('dep_id', $id)
			->delete('department');
	}
	public function deletePosition($id) {
		$this->db
			->where('psn_id', $id)
			->delete('position');
	}
	public function getAllStaffInPosition($id) {
		return $this->db
			->select()
			->where('sta_position_id', $id)
			->get('staff')
			-> result_array();
	}
}