<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mevent'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->view_all_events();
	}
		/**
		View event
	*/
	public function view_all_events() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Bảng tin sự kiện',
			'pageGroupTitle' => 'Sự kiện',
			'pageGroupId' => 'event',
			'events'=>$this->Mevent->getEvents(),
			);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/event/view',$data);
		$this->load->view('admin/foot');
	}
	/**
		Add event
	*/
	public function add_new_event() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Tạo tin mới',
			'pageGroupTitle' => 'Sự kiện',
			'pageGroupId' => 'event',
		);
		
		$this->form_validation->set_rules('date','Thời gian','required');
		$this->form_validation->set_rules('place','Địa điểm','required');
		$this->form_validation->set_rules('content','Nội dung','required');
		if($this->form_validation->run()===FALSE){
			$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/event/add',$data);
			$this->load->view('admin/foot');
		}
		else{
			$data= array(
				'eve_date' => $this->input->post('date'), 
				'eve_place'	=>$this->input->post('place'),
				'eve_content'=>$this->input->post('content'),
				
				);
			$this->Mevent->addNew($data);
			redirect( base_url('event/view_all_events')) ;
		}
	}

	/**
		Edit event
	*/
	public function edit_event($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Sửa tin',
			'pageGroupTitle' => 'Sự kiện',
			'pageGroupId' => 'event',
			'event' => $this ->Mevent->getEventById($id),
			'id' => $id, 
			);
		$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/event/edit',$data);
			$this->load->view('admin/foot');
	}
	public function updateEvent($id){
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->form_validation->set_rules('date','Thời gian','required');
		$this->form_validation->set_rules('place','Địa điểm','required');
		$this->form_validation->set_rules('content','Nội dung','required');
			if($this->form_validation->run()===FALSE){
			$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/event/edit');
			$this->load->view('admin/foot');
		}
		else{
			$data= array(
				'eve_date' => $this->input->post('date'), 
				'eve_place'	=>$this->input->post('place'),
				'eve_content'=>$this->input->post('content'),
				
				);
			$this->Mevent->updateEvent($id,$data);
			redirect( base_url('event')) ;
		}
	}
	/**
	DELETE Event
	 */
	public function delete_event($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->Mevent->delete($id);
		redirect(base_url('event'));
	}
}