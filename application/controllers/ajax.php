<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation'));
		$this->load->Model(array('Mstaff','Mposition','Mdepartment'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function get_position_by_department_id($id) {
		$result = $this->Mposition->getPositionByDepartmentId($id);
		foreach ($result as $key => $value) {
			echo '<option value="'.$value['psn_id'].'">'.$value['psn_name'].'</option>';
		}
	}
}