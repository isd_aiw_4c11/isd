<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bonusfine extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Msalary','Mattendance','Mstaff'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->calculate_salary();
	}
	/**
		Bonus & Fine
	*/
	public function manage_bonus_fine() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Quản lý thưởng phạt',
			'pageGroupTitle' => 'Bảng lương',
			'pageGroupId' => 'salary',
			'bfs' => $this->Msalary->getBonusFine()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/bonusfine/bonus_fine_list', $data);
		$this->load->view('admin/foot');
	}
	public function bonus_fine_detail($id = NULL) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Chi tiết thưởng phạt',
			'pageGroupTitle' => 'Bảng lương',
			'pageGroupId' => 'salary',
			'staffs' => $this->Mstaff->getStaffs()
		);
		if ($id != NULL) {
			$data['bfs'] = $this->Msalary->getBonusFine($id);
		}
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/bonusfine/bonus_fine_detail', $data);
		$this->load->view('admin/foot');
	}
	public function add_bonus_fine() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = $this->input->post('bnf');
		$this->Msalary->addBonusFine($data);
		redirect(base_url('bonusfine/manage_bonus_fine'));
	}
	public function update_bonus_fine() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = $this->input->post('bnf');
		$this->Msalary->updateBonusFine($data);
		redirect(base_url('bonusfine/bonus_fine_detail/'.$data['bnf_id']));
	}
	public function remove_bonus_fine($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->Msalary->deleteBonusFine($id);
		redirect(base_url('bonusfine/manage_bonus_fine'));
	}
}