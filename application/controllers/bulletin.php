<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bulletin extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mstaff','Mbulletin'));
		date_default_timezone_set("Asia/Bangkok");
	}

	public function index() {
		$this->view_all_bulletins();
	}
	public function view_all_bulletins() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
		'pageTitle' => 'Danh sách tin',
		'pageGroupTitle' => 'Bảng tin',
		'pageGroupId' => 'bulletin',
		'pageGroupId' => 'bulletin',
		'bulletin' => $this->Mbulletin->getBulletin(),
		);

		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/bulletin/view', $data);
		$this->load->view('admin/foot');
	}

	/**
	Add new Buletin
	*/
	public function add_new_bulletin() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Tạo tin mới',
			'pageGroupTitle' => 'Bảng tin',
			'pageGroupId' => 'bulletin',
			'staffs'=>$this->Mstaff->getStaffs(),
			);
		
		$this->form_validation->set_rules('title','Tiêu đề','required');
		$this->form_validation->set_rules('content','Nội dung','required');
		if($this->form_validation->run()===FALSE){
			$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/bulletin/add',$data);
			$this->load->view('admin/foot');
		}
		else{
			$time = date("Y-m-d H:i:s");
			$data= array(
				'pst_title' => $this->input->post('title'), 
				'pst_time'	=>$time,
				'pst_content'=>$this->input->post('content'),
			);
			$this->Mbulletin->addNew($data);
			redirect( base_url('bulletin') );
		}
	}
	/**
	DELETE Buletin
	 */
	public function delete_bulletin($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->Mbulletin->delete($id);
		redirect(base_url('bulletin'));
	}

	/**
		Edit Bulletin
	*/
	public function edit_bulletin($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Sửa tin',
			'pageGroupTitle' => 'Bảng tin',
			'pageGroupId' => 'bulletin',
			'bulletin' => $this ->Mbulletin->getPostById($id),
			'staffs'=>$this->Mstaff->getStaffs(),
			'id' => $id, 
			);
		$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/bulletin/edit',$data);
			$this->load->view('admin/foot');
	}
	public function updateBulletin($id){
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->form_validation->set_rules('title','Tiêu đề','required');
		$this->form_validation->set_rules('content','Nội dung','required');
			if($this->form_validation->run()===FALSE){
			$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/bulletin/edit');
			$this->load->view('admin/foot');
		}
		else{
			$time = date("Y-m-d H:i:s");
			$data= array(
				'pst_title' => $this->input->post('title'), 
				'pst_content'=>$this->input->post('content'),
				
				);
			$this->Mbulletin->updatePost($id,$data);
			redirect( base_url('bulletin')) ;

		}
	}
} // EO class 