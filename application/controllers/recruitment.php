<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recruitment extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mrecruitment'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->candidates();
	}
	/**
		Candidate list
	*/
	public function candidates() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Đơn ứng tuyển',
			'pageGroupTitle' => 'Tuyển dụng',
			'pageGroupId' => 'recruitment',
			'candidates' => $this->Mrecruitment->getAllCandidate()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/recruitment/candidates', $data);
		$this->load->view('admin/foot');
	}
	public function is_approved($command, $id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		switch ($command) {
			case 0:
				$this->Mrecruitment->deleteCandidate($id);
				break;
			case 1:
				$this->Mrecruitment->markCandidate($id, array('cdd_is_approved'=>'0'));
				break;
			case 2:
				$this->Mrecruitment->markCandidate($id, array('cdd_is_approved'=>'1'));
				break;
			default:
				break;
		}
		redirect('recruitment');
	}
	/**
		Recruitment
	*/
	public function news() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Bảng tin tuyển dụng',
			'pageGroupTitle' => 'Tuyển dụng',
			'pageGroupId' => 'recruitment',
			'newss' => $this->Mrecruitment->getRecruitmentNews()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/recruitment/news', $data);
		$this->load->view('admin/foot');
	}
	public function create_news() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Tạo tin tuyển dụng',
			'pageGroupTitle' => 'Tuyển dụng',
			'pageGroupId' => 'recruitment',
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/recruitment/create_news', $data);
		$this->load->view('admin/foot');
	}
	public function create_news_exec() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$post = $this->input->post('post');
		$post['pst_time'] = date('Y-m-d H:i:s');
		$post['pst_is_recruitment'] = '1';
		$this->Mrecruitment->createNews($post);
		redirect('recruitment/news');
	}
	public function edit_news($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Sửa tin tuyển dụng',
			'pageGroupTitle' => 'Tuyển dụng',
			'pageGroupId' => 'recruitment',
			'newss' => $this->Mrecruitment->getNews($id)
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/recruitment/edit_news', $data);
		$this->load->view('admin/foot');
	}
	public function update_news_exec() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$post = $this->input->post('post');
		$postId = $this->input->post('post_id');
		$this->Mrecruitment->updateNews($postId,$post);
		redirect('recruitment/news');
	}
	public function remove_news($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->Mrecruitment->deleteNews($id);
		redirect('recruitment/news');
	}
	public function recruitment_form() {
		$data = array(
			'pageTitle' => 'Dự tuyển',
			'pageGroupTitle' => 'Tuyển dụng',
			'pageGroupId' => 'recruitment',
			'provinces' => $this->Mrecruitment->getAllProvinces()
		);
		if ($this->session->flashdata('message') != null) {
			$data['flash_message'] = $this->session->flashdata('message');
		}
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('staff/sidebar', $data);
		$this->load->view('staff/recruitment/register', $data);
		$this->load->view('admin/foot');
	}
	public function recruitment_submit() {
		$data = $this->input->post('cdd');
		$this->Mrecruitment->submitRecruitment($data);
		$this->session->set_flashdata(array(
			'type' => 'info',
			'message' => 'Gửi thành công',
		));
		redirect(base_url('recruitment/recruitment_form'));
	}
}