<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mattendance','Mstaff','Mbulletin','Mrecruitment'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		if ($this->session->userdata('sta_permission') < 1) {
			redirect(base_url('welcome/staff'));
		}
		$data = array(
			'pageTitle' => 'Tổng quan',
			'pageGroupTitle' => 'Bảng điều khiển',
			'pageGroupId' => 'welcome',
			'posts' => $this->Mbulletin->getBulletin(),
			'recruitments' => $this->Mrecruitment->getRecruitmentNews()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/home.php',$data);
		$this->load->view('admin/foot');
	}
	public function staff() {
		$data = array(
			'pageTitle' => 'Trang chủ',
			'pageGroupTitle' => 'Công ty BMK',
			'pageGroupId' => 'welcome',
			'posts' => $this->Mbulletin->getBulletin(),
			'recruitments' => $this->Mrecruitment->getRecruitmentNews()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('staff/sidebar', $data);
		$this->load->view('staff/home', $data);
		$this->load->view('admin/foot');
	}
	/**
	Add new Buletin
	*/
	public function add_new_bulletin() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Tạo tin mới',
			'pageGroupTitle' => 'Bảng tin',
			'pageGroupId' => 'bulletin',
			'staffs'=>$this->Mstaff->getStaffs(),
			);
		
		$this->form_validation->set_rules('title','Tiêu đề','required');
		$this->form_validation->set_rules('content','Nội dung','required');
		if($this->form_validation->run()===FALSE){
			$this->load->view('admin/head');
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar', $data);
			$this->load->view('admin/bulletin/add',$data);
			$this->load->view('admin/foot');
		}
		else{
			$time = date("Y-m-d H:i:s");
			$data= array(
				'pst_title' => $this->input->post('title'), 
				'pst_time'	=>$time,
				'pst_content'=>$this->input->post('content'),
			);
			$this->Mbulletin->addNew($data);
			redirect( base_url('welcome') );
		}
	}
}