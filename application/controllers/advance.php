<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advance extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Msalary','Mattendance','Mstaff'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->calculate_salary();
	}
	/**
		Advance request
	*/
	public function request_advance() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Xin ứng tiền',
			'pageGroupTitle' => 'Tiền lương',
			'pageGroupId' => 'salary',
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('staff/sidebar', $data);
		$this->load->view('staff/salary/advance_request', $data);
		$this->load->view('admin/foot');
	}
	public function request_advance_exec() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$data = $this->input->post('adr');
		$this->Msalary->insertAdvanceRequest($data);
		redirect(base_url('welcome'));
	}
	public function manage_advance_request() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Quản lý yêu cầu ứng tiền',
			'pageGroupTitle' => 'Bảng lương',
			'pageGroupId' => 'salary',
			'requests' => $this->Msalary->getAdvancesRequest()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/advance/advance_list', $data);
		$this->load->view('admin/foot');
	}
	public function handle_advance_request($command, $id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$status = array(
			'adr_status' => $command,
		);
		$this->Msalary->handleAdvanceRequest($id, $status);
		redirect(base_url('advance/manage_advance_request'));
	}
}