<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mdepartment', 'Mstaff'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->manage_department();
	}
	/**
		Bonus & Fine
	*/
	public function manage_department() {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Quản lý phòng ban',
			'pageGroupTitle' => 'Phòng ban',
			'pageGroupId' => 'staff',
			'departments' => $this->Mdepartment->getAllDepartments(),
			'positions' => $this->Mdepartment->getAllPositions()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/department/manage', $data);
		$this->load->view('admin/foot');
	}
	public function add_department() {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$data = $this->input->post('dep');
		$this->Mdepartment->createDepartment($data);
		redirect(base_url('department'));
	}
	public function add_position() {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$data = $this->input->post('psn');
		$this->Mdepartment->createPosition($data);
		redirect(base_url('department'));
	}
	public function delete_department($id) {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$positions = $this->Mdepartment->getAllPositionsInDepartment($id);
		foreach ($positions as $key => $position) {
			$staffs = $this->Mdepartment->getAllStaffInPosition($position['psn_id']);
			foreach ($staffs as $key => $staff) {
				$this->Mstaff->fireStaff($staff['sta_id']);
			}
			$this->Mdepartment->deletePosition($position['psn_id']);
		}
		$this->Mdepartment->deleteDepartment($id);
		redirect(base_url('department'));
	}
	public function delete_position($id) {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$staffs = $this->Mdepartment->getAllStaffInPosition($id);
		foreach ($staffs as $key => $staff) {
			$this->Mstaff->fireStaff($staff['sta_id']);
		}
		$this->Mdepartment->deletePosition($id);
		redirect(base_url('department'));
	}
}