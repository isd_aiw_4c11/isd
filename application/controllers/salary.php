<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salary extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Msalary','Mattendance','Mstaff'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->calculate_salary();
	}
	/**
		Salary config
	*/
	public function config() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Tùy chỉnh thông số bảng lương',
			'pageGroupTitle' => 'Quản lý lương',
			'pageGroupId' => 'salary',
			'configs' => $this->Msalary->getSalaryConfig(),
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/salary/config', $data);
		$this->load->view('admin/foot');
	}
	public function config_update() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->Msalary->updateSalaryConfig($this->input->post('config'));
		redirect(base_url('salary/config'));
	}
	/**
		Calculate salary
	*/
	public function calculate_salary($urlMonth = NULL, $urlYear = NULL) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		// Get current month and year if no input found
		if ($this->input->post('month') != '' && $this->input->post('year') !='') {
			$month = $this->input->post('month');
			$year = $this->input->post('year');
		} elseif ($urlMonth != '' && $urlYear != '') {
			$month = $urlMonth;
			$year = $urlYear;
		} else {
			$month = date('m');
			$year = date('Y');
		}
		$data = array(
			'pageTitle' => 'Quản lý lương',
			'pageGroupTitle' => 'Bảng lương',
			'pageGroupId' => 'salary',
			'configs' => $this->Msalary->getSalaryConfig(),
			'staffs' => $this->Mstaff->getStaffs(),
			'attendances' => $this->Msalary->getAttendancesByMonth($month, $year),
			'bfs' => $this->Msalary->getBonusFineByMonth($month, $year),
			'advances' => $this->Msalary->getAdvanceRequestByMonth($month, $year),
			'month' => $month,
			'year' => $year
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/salary/all', $data);
		$this->load->view('admin/foot');
	}
}