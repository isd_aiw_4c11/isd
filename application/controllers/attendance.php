<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendance extends CI_Controller {
	public $daysInMonth = array (
		'01' => '31',
		'02' => '28',
		'03' => '31',
		'04' => '30',
		'05' => '31',
		'06' => '30',
		'07' => '31',
		'08' => '31',
		'09' => '30',
		'10' => '31',
		'11' => '30',
		'12' => '31',
	);
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mattendance','Mstaff','Msalary'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->view_attendance();
	}
	/**
		View attendance
	*/
	public function view_attendance($urlMonth = NULL, $urlYear = NULL) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		// Get current month and year if no input found
		if ($this->input->post('month') != '' && $this->input->post('year') !='') {
			$month = $this->input->post('month');
			$year = $this->input->post('year');
		} elseif ($urlMonth != '' && $urlYear != '') {
			$month = $urlMonth;
			$year = $urlYear;
		} else {
			$month = date('m');
			$year = date('Y');
		}

		$data = array(
			'pageTitle' => 'Bảng chấm công',
			'pageGroupTitle' => 'Chấm công',
			'pageGroupId' => 'attendance',
			'daysInMonth'=>$this->daysInMonth,
			'staffs' => $this->Mstaff->getStaffs(),
			'attendances' => $this->Mattendance->getAttendancesByMonth($month, $year),
			'month' => $month,
			'year' => $year,
			'configs' => $this->Msalary->getSalaryConfig(),
		);

		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/attendance/view', $data);
		$this->load->view('admin/foot');
	}
	/**
		Generate attendance records
	*/
	public function add_attendance_table() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Tạo bảng chấm công',
			'pageGroupTitle' => 'Chấm công',
			'pageGroupId' => 'attendance',
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/attendance/add-step-1', $data);
		$this->load->view('admin/foot');
	}
	public function add_attendance_table_step_2() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		// Get month
		$month = $this->input->post('month');
		// Get year
		$year = $this->input->post('year');
		// Get available staffs for add new attendance record
		$availableStaffs = $this->__getAvailableStaffForAddAttendance($month, $year);

		$data = array(
			'pageTitle' => 'Tạo bảng chấm công',
			'pageGroupTitle' => 'Chấm công',
			'pageGroupId' => 'attendance',
			'availableStaffs' => $availableStaffs,
			'month' => $month,
			'year' => $year,
		);

		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/attendance/add-step-2', $data);
		$this->load->view('admin/foot');
	}
	public function add_attendance_table_exe() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$staffs = $this->input->post('staffs');
		$this->Mattendance->createAttendanceTable($staffs, $month, $year);
		redirect( base_url('attendance/view_attendance') );
	}
	/**
		Check attendance
	*/
	public function check_attendance ($day, $month, $year) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Chấm công',
			'pageGroupTitle' => 'Chấm công',
			'pageGroupId' => 'attendance',
			'day' => $day,
			'month' => $month,
			'year' => $year,
			'staffsAndPoints' => $this->Mattendance->getAttendancesByDay($day, $month, $year)
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/attendance/check', $data);
		$this->load->view('admin/foot');
	}
	public function check_attendance_exe () {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$day = $this->input->post('day');
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$date = $year.'-'.str_pad($month, 2, '0', STR_PAD_LEFT).'-'.str_pad($day, 2, '0', STR_PAD_LEFT);
		$staffPoint = $this->input->post('staffs');
		$this->Mattendance->checkAttendance($date, $staffPoint);
		redirect( base_url('attendance/view_attendance/'.$month.'/'.$year) );
	}
	/**
		Helper functions
	*/
	// private function decodeDate($inputDate) {
	// 	$day = date( 'd', strtotime($inputDate) );
	// 	$month = date( 'm', strtotime($inputDate) );
	// 	$year = date( 'Y', strtotime($inputDate) );
	// 	return $outputDate = array(
	// 		'd' => $day,
	// 		'm' => $month,
	// 		'Y' => $year
	// 	);
	// }
	public function __getAvailableStaffForAddAttendance($month, $year) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		// Initialize posible staff for add attendance
		$availableStaffs = array();
		// Get all staffs
		$staffs = $this->Mstaff->getStaffs();
		// Get staffs that haven't been created attendance
		foreach ($staffs as $key => $staff) {
			$staffsNumber = $this->Mstaff->getStaffWithAttendance($staff['sta_id'], $month, $year);
			// If result return no staff with that month and year, add to $avilableStaffs
			if (count($staffsNumber) == 0) {
				array_push($availableStaffs, $staffs[$key]);
			}
		}
		return $availableStaffs;
	}
}