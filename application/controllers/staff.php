<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mstaff','Mposition','Mdepartment'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$this->view_staffs();
	}
	/**
		View all staff
	*/
	public function view_staffs() {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Danh sách nhân viên',
			'pageGroupTitle' => 'Nhân viên',
			'pageGroupId' => 'staff',
			'staffs' => $this->Mstaff->getStaffs(),
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/staff/all', $data);
		$this->load->view('admin/foot');
	}
	/**
		View single staff
	*/
	public function staff_detail($id) {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Chi tiết nhân viên',
			'pageGroupTitle' => 'Nhân viên',
			'pageGroupId' => 'staff',
			'staff' => $this->Mstaff->getStaffsById($id),
			'positions' => $this->Mposition->getAllPositions(),
			'departments' => $this->Mdepartment->getAllDepartments(),
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/staff/detail', $data);
		$this->load->view('admin/foot');
	}
	public function current_profile_detail() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Đổi thông tin',
			'pageGroupTitle' => 'Nhân viên',
			'pageGroupId' => 'staff',
			'staff' => $this->Mstaff->getStaffsById($this->session->userdata('sta_id')),
			'positions' => $this->Mposition->getAllPositions(),
			'departments' => $this->Mdepartment->getAllDepartments(),
		);
		if ($this->session->flashdata('message') != null) {
			$data['flash_message'] = $this->session->flashdata('message');
		}
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('staff/sidebar', $data);
		$this->load->view('staff/staff/detail', $data);
		$this->load->view('admin/foot');
	}
	/**
		Add new staff
	*/
	public function add_staff($temp = null) {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Thêm nhân viên',
			'pageGroupTitle' => 'Nhân viên',
			'pageGroupId' => 'staff',
			'positions' => $this->Mposition->getAllPositions(),
			'departments' => $this->Mdepartment->getAllDepartments(),
		);
		if($temp==null) {
			$data['temp'] = array(
				'sta_id' => '',
				'sta_firstname' => '',
				'sta_lastname' => '',
				'sta_civilianId' => '',
				'sta_dob' => '',
				'sta_hometown' => '',
				'sta_address' => '',
				'sta_email' => '',
				'sta_phone' => '',
				'sta_staff_id' => '',
				'sta_position_id' => '',
				'sta_contract_deadline' => '',
			);
		} else {
			$data['temp'] = $temp;
		}
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/staff/add', $data);
		$this->load->view('admin/foot');
	}
	public function add_staff_exec() {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$post = $this->input->post('sta');
		$departmentId = $this->input->post('sta_department_id');
		$newStaffId = $this->Mstaff->getLatestStaff()[0]['sta_id'] + 1;
		$genUsername = str_pad($departmentId, 2, '0', STR_PAD_LEFT).str_pad($post['sta_position_id'], 2, '0', STR_PAD_LEFT).str_pad($newStaffId, 4, '0', STR_PAD_LEFT);
		$post['sta_username'] = $genUsername;
		$post['sta_password'] = md5($genUsername);
		$this->Mstaff->addStaff($post);
		redirect( base_url('staff' ));
	}
	/**
		Update staff
	*/
	public function update_profile() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$data = $this->input->post('sta');
		if ($data['sta_password'] != '') {
			$data['sta_password'] = md5($data['sta_password']);
		} else {
			unset($data['sta_password']);
		}
		$this->Mstaff->updateProfile($data);
		redirect( base_url('staff') );
	}
	public function update_current_profile() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$post = $this->input->post('sta');
		$data['sta_id'] = $this->session->userdata('sta_id');
		$data['sta_firstname'] = $post['sta_firstname'];
		$data['sta_lastname'] = $post['sta_lastname'];
		$data['sta_address'] = $post['sta_address'];
		$data['sta_phone'] = $post['sta_phone'];
		$data['sta_email'] = $post['sta_email'];
		if ($post['sta_password'] != '') {
			$data['sta_password'] = md5($post['sta_password']);
		}
		$this->Mstaff->updateProfile($data);
		$this->session->set_flashdata(array(
			'type' => 'info',
			'message' => 'Cập nhật thành công',
		));
		redirect( base_url('staff/current_profile_detail') );
	}
	/**
		Fire staff
	*/
	public function fire_staff($id) {
		bmk_session_check('admin_2',$this->session->userdata('sta_permission'));
		$this->Mstaff->fireStaff($id);
		redirect( base_url('staff') );
	}
	/**
		Login
	*/
	public function login() {
		if($this->session->userdata('sta_id') != null) {
			redirect(base_url('welcome'));
		}
		$this->load->view('admin/authen/login');
	}
	public function authen() {
		if($this->session->userdata('sta_id') != null) {
			redirect(base_url());
		}
		$post = array(
			'sta_username' => $this->input->post('username'),
			'sta_password' => md5($this->input->post('password'))
		);
		$users = $this->Mstaff->login($post);
		if(sizeof($users) != 0) {
			foreach ($users as $key => $user) {
				$user['sta_fullname'] = $user['sta_firstname'].' '.$user['sta_lastname'];
				$this->session->set_userdata($user);
			}
			redirect(base_url());
		} else {
			$this->session->set_flashdata('message','Tên đăng nhập hoặc mật khẩu sai');
			redirect(base_url('staff/login'));
		}
	}
	/**
	 Log out
	 */
	public function logout() {
		if($this->session->userdata('sta_id') != null) {
			$this->session->sess_destroy();
			redirect(base_url('staff/login'));
		} else {
			redirect(base_url());
		}
	}
}