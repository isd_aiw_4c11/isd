<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absence extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('html','url','form'));
		$this->load->library(array('pagination','form_validation','session'));
		$this->load->Model(array('Mattendance','Mstaff'));
		date_default_timezone_set("Asia/Bangkok");
	}
	public function index() {
		$this->view_absence_requests();
	}
	/**
		Absence
	*/
	public function request_absence() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Xin nghỉ phép',
			'pageGroupTitle' => 'Nghỉ phép',
			'pageGroupId' => 'absence',
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('staff/sidebar', $data);
		$this->load->view('staff/absence/absence_request', $data);
		$this->load->view('admin/foot');
	}
	public function request_absence_exec() {
		bmk_session_check('staff',$this->session->userdata('sta_permission'));
		$data = $this->input->post('abr');
		$this->Mattendance->insertAbsenceRequest($data);
		redirect(base_url('welcome'));
	}
	public function view_absence_requests() {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$data = array(
			'pageTitle' => 'Đơn nghỉ phép',
			'pageGroupTitle' => 'Quản lý nghỉ phép',
			'pageGroupId' => 'absence',
			'abrs' => $this->Mattendance->getAllAbsenceRequests()
		);
		$this->load->view('admin/head');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar', $data);
		$this->load->view('admin/absence/list', $data);
		$this->load->view('admin/foot');
	}
	public function handle_absence_request($command, $id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		switch ($command) {
			case '0':
				$this->Mattendance->handleAbsenceRequest($id,array('abr_is_approved' => '0' ));
				break;
			case '1':
				$this->Mattendance->handleAbsenceRequest($id,array('abr_is_approved' => '1' ));
				break;
		}
		redirect('absence/view_absence_requests');
	}
	public function remove_absence_request($id) {
		bmk_session_check('admin_1',$this->session->userdata('sta_permission'));
		$this->Mattendance->deleteAbsenceRequest($id);
		redirect('absence/view_absence_requests');
	}
}